package multi.platform.ui.example.app

import android.content.SharedPreferences
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.ui.example.R
import org.koin.core.component.inject
import multi.platform.ui.shared.app.common.BaseActivity
import multi.platform.example_lib.shared.R as eR

class MainActivity : BaseActivity() {
    private val sharedPreferences: SharedPreferences by inject()

    override fun appVersion() = getString(R.string.app_version)
    override fun actionBarExpandedAutoCompleteHint() = eR.string.search_hint
    override fun actionBarSearchHint() = eR.string.search_hint
    override fun bottomNavBarMenu() = eR.menu.menu_bottom
    override fun topLevelDestinations(): Set<Int> {
        val list = HashSet<Int>()
        list.add(eR.id.homeFragment)
        list.add(eR.id.searchFragment)
        list.add(eR.id.favoriteFragment)
        return list
    }
    override fun navGraph(): Int {
        val onBoarding = sharedPreferences.getBoolean(AppConstant.ONBOARDING_KEY, false)
        return if (onBoarding) R.navigation.main_nav_graph else eR.navigation.onboarding_nav_graph
    }

}