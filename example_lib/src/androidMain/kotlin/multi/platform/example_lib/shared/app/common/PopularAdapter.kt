package multi.platform.example_lib.shared.app.common

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.example_lib.shared.databinding.PopularItemBinding
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.ui.shared.R
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder

class PopularAdapter : BaseAdapter<Movie>() {
    override var empty = Movie()
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> {
        return ViewHolder(
            PopularItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(
        val binding: PopularItemBinding,
    ) : BaseViewHolder<Movie>(binding.root, radius, elevation, onSelected, onTouch) {
        override val shine = binding.shinePopular
        override val cardView = binding.cvPopular

        override fun onLoading() {
            binding.apply {
                title.apply {
                    tvTitle.apply {
                        text = "               "
                        setBackgroundResource(R.color.skeleton)
                    }
                    tvRate.apply {
                        text = "   "
                        setBackgroundResource(R.color.skeleton)
                        setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    tvSubtitle.apply {
                        text = "                    "
                        setBackgroundResource(R.color.skeleton)
                    }
                }
                ivPlay.apply {
                    setBackgroundResource(R.color.skeleton)
                    setImageResource(0)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        override fun onSuccess(item: Movie) {
            binding.apply {
                ivPopular.loadImage(
                    item.backdropPath ?: R.drawable.ic_image,
                    RequestOptions().centerCrop()
                )
                ivPlay.apply {
                    setBackgroundResource(android.R.color.transparent)
                    setImageResource(R.drawable.ic_play_primary_circle)
                }
                title.apply {
                    tvTitle.apply {
                        text = item.title
                        setBackgroundResource(android.R.color.transparent)
                    }
                    tvRate.apply {
                        text = String.format("%.1f", item.voteAverage?.toDouble())
                        setBackgroundResource(R.drawable.bg_white20_round8)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_star, 0, 0, 0)
                    }
                    tvSubtitle.apply {
                        item.genres.takeIf { it.isNotEmpty() }?.let { g ->
                            val hour = item.runtime / 60
                            val minute = item.runtime % 60
                            text = "${hour}h ${minute}min | ${g[0].name}"
                        }
                        setBackgroundResource(android.R.color.transparent)
                    }
                }
            }
        }

        override fun onEmpty() {
            binding.apply {
                cvPopular.isVisible = false
                title.apply {
                    tvRate.isVisible = false
                    tvSubtitle.isVisible = false
                    tvTitle.text = root.context.getString(R.string.empty)
                }
            }
        }
    }
}