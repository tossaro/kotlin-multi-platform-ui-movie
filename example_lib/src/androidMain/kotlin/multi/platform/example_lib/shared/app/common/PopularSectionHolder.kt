package multi.platform.example_lib.shared.app.common

import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavOptions
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import multi.platform.core.shared.external.extension.launchAndCollectIn
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.databinding.PopularSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.external.enum.MoviesType
import multi.platform.ui.shared.app.common.BaseViewHolder
import multi.platform.ui.shared.external.utility.State
import org.koin.java.KoinJavaComponent.inject
import kotlin.math.abs

class PopularSectionHolder(
    val binding: PopularSectionViewBinding,
    private val lifecycleOwner: LifecycleOwner,
    private val swipeRefreshLayout: SwipeRefreshLayout?,
    private val isInternetAvailable: () -> Boolean,
    private val hideKeyboard: () -> Unit,
    private val showSnackbar: ((String?, Boolean) -> Unit)? = null,
    private val showToast: ((String?) -> Unit)? = null,
    private val goTo: ((String, NavOptions?) -> Unit)? = null,
    private val extra: Any? = null,
) : BaseViewHolder<Section>(binding.root) {
    private val moviesViewModel: MoviesViewModel by inject(MoviesViewModel::class.java)
    private val movieDetailViewModel: MovieDetailViewModel by inject(MovieDetailViewModel::class.java)

    private var delay = 2000L
    private var handler: Handler? = Handler(Looper.getMainLooper())
    private var runnable: Runnable? = Runnable {
        binding.vpPopular.apply {
            adapter?.itemCount?.let {
                currentItem = if (currentItem == it - 1) 0 else currentItem + 1
            }
        }
    }

    private lateinit var listAdapter: PopularAdapter

    override fun bind(item: Section, state: State?) {
        setupAdapter()
        setupObserver()
        load(0)
        super.bind(item, state)
    }

    private fun setupObserver() {
        binding.lifecycleOwner = lifecycleOwner
        binding.vmMovies = moviesViewModel.also {
            it.limit = 5
            it.search = extra
            it.type = MoviesType.POPULAR
            it.lists.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { ss ->
                handleMovies(ss)
                it.lists.value = null
            }
            it.loadingIndicator.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { l ->
                onLoading(l)
            }
            it.successMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast?.invoke(m)
                it.toastMessage.value = null
            }
            it.onServerError.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { ok ->
                if (ok) listAdapter.showError()
                it.onServerError.value = false
            }
        }
        binding.vmMovieDetail = movieDetailViewModel.also {
            it.successMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast?.invoke(m)
                it.toastMessage.value = null
            }
            it.movie.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                notifyAdapter(m)
                it.movie.value = null
            }
        }
    }

    private fun setupAdapter() {
        listAdapter = PopularAdapter().apply {
            radius = 7
            onSelected = { _, it ->
                goTo?.invoke(
                    binding.root.context.getString(R.string.route_detail)
                        .replace("{id}", it.id.toString()),
                    null
                )
            }
            fetchData = {
                moviesViewModel.page++
                load(moviesViewModel.page)
            }
            onTouch = { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        runnable?.let { handler?.removeCallbacks(it) }
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        runnable?.let { handler?.postDelayed(it, delay) }
                        false
                    }
                    else -> v.onTouchEvent(event)
                }
            }
        }
        binding.vpPopular.apply {
            adapter = listAdapter
            isNestedScrollingEnabled = false
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if (listAdapter.state == State.SUCCESS) {
                        runnable?.let {
                            handler?.removeCallbacks(it)
                            handler?.postDelayed(it, delay)
                        }
                    }
                }
            })

            val transformer = CompositePageTransformer()
            transformer.addTransformer(MarginPageTransformer(40))
            transformer.addTransformer { v, p ->
                v.scaleY = 0.85f + (1 - abs(p)) * 0.15f
            }
            setPageTransformer(transformer)
            TabLayoutMediator(binding.tlPopular, this) { _, _ -> }.attach()
        }
    }

    private fun onLoading(isLoading: Boolean?) {
        hideKeyboard()
        if (listAdapter.itemsCache.size == 0 && isLoading == true) listAdapter.showSkeletons(
            moviesViewModel.limit
        )
    }

    private fun load(p: Int) {
        moviesViewModel.page = p
        moviesViewModel.isFromNetwork = isInternetAvailable()
        if (p == 1) listAdapter.clear()
        moviesViewModel.load()
    }

    private fun handleMovies(movies: List<Movie>?) {
        movies?.let { its ->
            binding.vpPopular.post {
                listAdapter.apply {
                    addItem {
                        items.addAll(its.take(moviesViewModel.limit))
                        itemsCache.addAll(its.take(moviesViewModel.limit))
                    }
                }
                if (isInternetAvailable()) {
                    its.take(moviesViewModel.limit).filter { it.runtime == 0 }.forEach {
                        movieDetailViewModel.getFromNetwork(it.id)
                    }
                }
            }
        }
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun notifyAdapter(movie: Movie?) {
        movie?.takeIf { it.popularOrder != -1 && it.popularOrder < listAdapter.itemCount }
            ?.let { m ->
                listAdapter.items[movie.popularOrder] = m
                listAdapter.itemsCache[movie.popularOrder] = m
                binding.vpPopular.post { listAdapter.notifyItemChanged(m.popularOrder) }
            }
    }
}