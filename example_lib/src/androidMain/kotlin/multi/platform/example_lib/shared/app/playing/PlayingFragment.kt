package multi.platform.example_lib.shared.app.playing

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.goTo
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.extension.showToast
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.app.common.MoviesSectionHolder
import multi.platform.example_lib.shared.databinding.MoviesSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.external.extension.showSnackbar


class PlayingFragment : BaseFragment<MoviesSectionViewBinding>(MoviesSectionViewBinding::inflate) {
    override fun actionBarTitle() = getString(R.string.playing_title)

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val moviesSectionHolder = MoviesSectionHolder(
            binding,
            viewLifecycleOwner,
            swipeRefreshLayout(),
            (requireActivity() as CoreActivity)::isInternetAvailable,
            ::dpToPx,
            ::hideKeyboard,
            ::showSnackbar,
            ::showToast,
            ::goTo
        ).also {
            actionBarSearch()?.apply {
                it.bind(Section().apply {
                    slug = "now_playing"; gridCount = 2
                })
            }
        }
        swipeRefreshLayout()?.setOnRefreshListener {
            moviesSectionHolder.load(0)
        }
    }
}