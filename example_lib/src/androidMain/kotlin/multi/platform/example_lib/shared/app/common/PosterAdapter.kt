package multi.platform.example_lib.shared.app.common

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.ui.shared.R
import multi.platform.example_lib.shared.databinding.PosterItemBinding
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder

class PosterAdapter : BaseAdapter<Movie>() {
    override var empty = Movie()
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> {
        return ViewHolder(
            PosterItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(
        val binding: PosterItemBinding,
    ) : BaseViewHolder<Movie>(binding.root, radius, elevation, onSelected, onTouch) {
        override val shine = binding.shinePoster
        override val cardView = binding.cvPoster

        @SuppressLint("RestrictedApi")
        override fun onLoading() {
            binding.apply {
                title.apply {
                    tvTitle.apply {
                        setBackgroundResource(R.color.skeleton)
                    }
                    tvRate.apply {
                        isVisible = true
                        setBackgroundResource(R.color.skeleton)
                        setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    tvSubtitle.apply {
                        isVisible = true
                        setBackgroundResource(R.color.skeleton)
                    }
                }
            }
        }

        @SuppressLint("SetTextI18n")
        override fun onSuccess(item: Movie) {
            binding.apply {
                ivPoster.loadImage(
                    item.posterPath ?: R.drawable.ic_image,
                    RequestOptions().centerCrop()
                )
                title.apply {
                    tvTitle.apply {
                        text = item.title
                        setBackgroundResource(android.R.color.transparent)
                    }
                    tvRate.apply {
                        isVisible = true
                        setBackgroundResource(R.drawable.bg_white20_round8)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_star, 0, 0, 0)
                        text = String.format("%.1f", item.voteAverage?.toDouble())
                    }
                    tvSubtitle.apply {
                        isVisible = true
                        setBackgroundResource(android.R.color.transparent)
                        item.genres.takeIf { it.isNotEmpty() }?.let { g ->
                            val hour = item.runtime / 60
                            val minute = item.runtime % 60
                            text = "${hour}h ${minute}min | ${g[0].name}"
                        }
                    }
                }
            }
        }

        override fun onEmpty() {
            binding.apply {
                cvPoster.isVisible = false
                title.apply {
                    tvRate.isVisible = false
                    tvSubtitle.isVisible = false
                    tvTitle.text = root.context.getString(R.string.empty)
                }
            }
        }
    }
}