package multi.platform.example_lib.shared.app.onboarding

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.launchAndCollectIn
import multi.platform.example_lib.shared.databinding.OnBoardingFragmentBinding
import multi.platform.ui.shared.app.common.BaseActivity
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.external.extension.showErrorSnackbar
import multi.platform.ui.shared.external.extension.showSuccessSnackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingFragment :
    BaseFragment<OnBoardingFragmentBinding>(OnBoardingFragmentBinding::inflate) {
    private val vm: OnBoardingViewModel by viewModel()

    override fun showActionBar() = false
    override fun enableSwipeRefresher() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                showFullLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.errorMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.onNext.launchAndCollectIn(this, Lifecycle.State.STARTED) { ok ->
                onNext(ok)
                it.onNext.value = false
            }
        }
    }

    private fun onNext(next: Boolean) {
        if (next) {
            sharedPreferences.edit().putBoolean(AppConstant.ONBOARDING_KEY, true).apply()
            (activity as BaseActivity).navGraph()?.let { g ->
                findNavController().graph.clear()
                findNavController().setGraph(g)
            }
        }
    }
}
