package multi.platform.example_lib.shared.app.detail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.ui.shared.R
import multi.platform.example_lib.shared.databinding.CastItemBinding
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder

class CastAdapter : BaseAdapter<MovieCredit>() {
    override var empty = MovieCredit()
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<MovieCredit> {
        return ViewHolder(
            CastItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(
        val binding: CastItemBinding,
    ) : BaseViewHolder<MovieCredit>(binding.root, radius, elevation, onSelected, onTouch) {
        override val shine = binding.shineCast

        @SuppressLint("RestrictedApi")
        override fun onLoading() {
            binding.apply {
                cvCast.isVisible = true
                tvDepartment.apply {
                    isVisible = true
                    setBackgroundResource(R.color.skeleton)
                }
                tvName.apply {
                    setBackgroundResource(R.color.skeleton)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        override fun onSuccess(item: MovieCredit) {
            binding.apply {
                cvCast.isVisible = true
                ivCast.loadImage(
                    item.profilePath ?: R.drawable.ic_image,
                    RequestOptions().centerCrop()
                )
                tvDepartment.apply {
                    isVisible = true
                    text = item.knownForDepartment
                    setBackgroundResource(android.R.color.transparent)
                }
                tvName.apply {
                    text = item.name
                    setBackgroundResource(android.R.color.transparent)
                }
            }
        }

        override fun onEmpty() {
            binding.apply {
                cvCast.isVisible = false
                tvDepartment.isVisible = false
                tvName.text = root.context.getString(R.string.empty)
            }
        }
    }
}