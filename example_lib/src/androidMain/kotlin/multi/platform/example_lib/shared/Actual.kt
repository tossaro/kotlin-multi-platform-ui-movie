package multi.platform.example_lib.shared

import multi.platform.core.shared.domain.common.CommonRepository
import multi.platform.example_lib.shared.app.common.MovieCreditsViewModel
import multi.platform.example_lib.shared.app.common.MovieDetailViewModel
import multi.platform.example_lib.shared.app.onboarding.OnBoardingViewModel
import multi.platform.example_lib.shared.app.common.MoviesViewModel
import multi.platform.example_lib.shared.data.common.CommonRepositoryImpl
import multi.platform.example_lib.shared.data.movie.MovieRepositoryImpl
import multi.platform.example_lib.shared.domain.movie.MovieRepository
import multi.platform.example_lib.shared.domain.movie.usecase.*
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

actual fun libModule() = module {
    single<CommonRepository> { CommonRepositoryImpl(get()) }
    viewModelOf(::OnBoardingViewModel)
    singleOf(::provideRealm)

    single<MovieRepository> { MovieRepositoryImpl(BuildConfig.APIKEY) }
    singleOf(::GetMovieUseCase)
    singleOf(::GetMovieLocalUseCase)
    singleOf(::SetMovieLocalUseCase)
    singleOf(::GetMovieVideosUseCase)

    singleOf(::GetMovieCreditsUseCase)
    singleOf(::GetMovieCreditsLocalUseCase)
    singleOf(::SetMovieCreditsLocalUseCase)

    singleOf(::GetMoviesUseCase)
    singleOf(::GetMoviesLocalUseCase)
    singleOf(::SetMoviesLocalUseCase)

    singleOf(::GetPopularMoviesUseCase)
    singleOf(::GetNowPlayingMoviesUseCase)
    singleOf(::GetUpcomingMoviesUseCase)
    singleOf(::GetSimilarMoviesUseCase)
    singleOf(::GetRecommendationMoviesUseCase)

    viewModelOf(::MovieDetailViewModel)
    viewModelOf(::MoviesViewModel)
    viewModelOf(::MovieCreditsViewModel)
}