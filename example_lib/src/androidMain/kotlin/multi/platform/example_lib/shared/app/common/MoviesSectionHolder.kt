package multi.platform.example_lib.shared.app.common

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavOptions
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import multi.platform.core.shared.external.extension.launchAndCollectIn
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.databinding.MoviesSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.external.enum.MoviesType
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder
import multi.platform.ui.shared.external.utility.GridSpacingItemDecoration
import multi.platform.ui.shared.external.utility.LinearSpacingItemDecoration
import multi.platform.ui.shared.external.utility.State
import org.koin.java.KoinJavaComponent.inject

class MoviesSectionHolder(
    val binding: MoviesSectionViewBinding,
    private val lifecycleOwner: LifecycleOwner,
    private val swipeRefreshLayout: SwipeRefreshLayout?,
    private val isInternetAvailable: () -> Boolean,
    private val dpToPx: (Float) -> Int,
    private val hideKeyboard: () -> Unit,
    private val showSnackbar: ((String?, Boolean) -> Unit)? = null,
    private val showToast: ((String?) -> Unit)? = null,
    private val goTo: ((String, NavOptions?) -> Unit)? = null,
    extra: Any? = null,
) : BaseViewHolder<Section>(binding.root) {
    private val moviesViewModel: MoviesViewModel by inject(MoviesViewModel::class.java)
    private val movieDetailViewModel: MovieDetailViewModel by inject(MovieDetailViewModel::class.java)

    private lateinit var listAdapter: BaseAdapter<Movie>
    var search = extra

    private fun movieType(slug: String?) : MoviesType = when (slug) {
        "popular_movies" -> MoviesType.POPULAR
        "latest_movies" -> MoviesType.UPCOMING
        "now_playing" -> MoviesType.NOW_PLAYING
        "loved" -> MoviesType.LOVED
        "search" -> MoviesType.SEARCH
        "similar" -> MoviesType.SIMILAR
        "recommendation" -> MoviesType.RECOMMENDATION
        else -> MoviesType.UNDEFINED
    }

    override fun bind(item: Section, state: State?) {
        val isLimited = item.limit != 0
        setupAdapter(isLimited, item)
        setupObserver(item)

        item.title?.let {
            binding.apply {
                tvTitle.apply { text = it; isVisible = true }
                rvMovies.updateLayoutParams<ConstraintLayout.LayoutParams> {
                    topMargin = dpToPx(16f)
                }
            }
        }

        binding.tvMore.apply {
            isVisible =
                isLimited && moviesViewModel.type != MoviesType.SIMILAR &&
                        moviesViewModel.type != MoviesType.RECOMMENDATION
            setOnClickListener {
                goTo?.invoke(context.getString( when (moviesViewModel.type) {
                    MoviesType.UPCOMING -> R.string.route_latest
                    MoviesType.NOW_PLAYING -> R.string.route_playing
                    else -> R.string.route_home
                }), null)
            }
        }

        load(0)
    }

    private fun setupObserver(item: Section) {
        binding.lifecycleOwner = lifecycleOwner
        binding.vmMovies = moviesViewModel.also {
            it.search = search
            if (item.limit > 0) it.limit = item.limit
            it.type = movieType(item.slug)
            it.lists.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { ss ->
                handleMovies(ss)
                it.lists.value = null
            }
            it.loadingIndicator.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { l ->
                onLoading(l)
            }
            it.successMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast?.invoke(m)
                it.toastMessage.value = null
            }
            it.onServerError.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { ok ->
                if (ok) listAdapter.showError()
                it.onServerError.value = false
            }
        }
        binding.vmMovieDetail = movieDetailViewModel.also {
            it.successMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar?.invoke(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast?.invoke(m)
                it.toastMessage.value = null
            }
            it.movie.launchAndCollectIn(lifecycleOwner, Lifecycle.State.STARTED) { m ->
                notifyAdapter(m)
                it.movie.value = null
            }
        }
    }

    private fun setupAdapter(isLimited: Boolean, item: Section) {
        val moviesAdapter = when (movieType(item.slug)) {
            MoviesType.SIMILAR, MoviesType.RECOMMENDATION -> RelatedAdapter()
            MoviesType.LOVED -> LovedAdapter()
            else -> PosterAdapter()
        }
        moviesAdapter.apply {
            widthRatio = item.widthRatio
            radius = 7
            onSelected = { _, it ->
                goTo?.invoke(
                    binding.root.context.getString(R.string.route_detail)
                        .replace("{id}", it.id.toString()),
                    null
                )
            }
            fetchData = {
                moviesViewModel.page++
                load(moviesViewModel.page)
            }
        }
        listAdapter = moviesAdapter
        binding.rvMovies.apply {
            adapter = listAdapter
            isNestedScrollingEnabled = item.enableNestingScroll
            layoutManager = if (item.gridCount == 0) LinearLayoutManager(
                context,
                if (item.orientation == 0) RecyclerView.HORIZONTAL else RecyclerView.VERTICAL,
                false
            ) else GridLayoutManager(context, item.gridCount)
            if (itemDecorationCount > 0) removeItemDecorationAt(0)
            if (!isLimited) addOnScrollListener(listAdapter.Listener(swipeRefreshLayout))
            addItemDecoration(
                if (item.gridCount == 0) LinearSpacingItemDecoration(
                    item.orientation,
                    dpToPx(item.spacingMidle),
                    dpToPx(item.spacingStart),
                    dpToPx(item.spacingEnd),
                    dpToPx(item.spacingOther),
                )
                else GridSpacingItemDecoration(
                    item.gridCount,
                    dpToPx(item.spacingStart),
                    dpToPx(item.spacingMidle),
                    dpToPx(item.spacingEnd),
                )
            )
        }
    }

    private fun onLoading(isLoading: Boolean?) {
        hideKeyboard()
        if (listAdapter.itemsCache.size == 0 && isLoading == true) listAdapter.showSkeletons(
            moviesViewModel.limit
        )
    }

    fun load(p: Int) {
        moviesViewModel.page = p
        moviesViewModel.isFromNetwork = isInternetAvailable()
        if (p == 0) listAdapter.clear()
        moviesViewModel.load()
    }

    private fun handleMovies(movies: List<Movie>?) {
        movies?.let { its ->
            binding.rvMovies.post {
                listAdapter.apply {
                    addItem {
                        items.addAll(its)
                        itemsCache.addAll(its)
                    }
                }
                if (isInternetAvailable()) {
                    its.filter { it.runtime == 0 }.forEach {
                        movieDetailViewModel.getFromNetwork(it.id)
                    }
                }
            }
        }
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun notifyAdapter(movie: Movie?) {
        movie?.let {
            val order: Int = when (moviesViewModel.type) {
                MoviesType.POPULAR -> it.popularOrder
                MoviesType.UPCOMING -> it.upcomingOrder
                MoviesType.NOW_PLAYING -> it.nowPlayingOrder
                MoviesType.LOVED -> it.lovedOrder
                MoviesType.SEARCH -> it.searchOrder
                MoviesType.SIMILAR -> it.similarOrder
                MoviesType.RECOMMENDATION -> it.recommendationOrder
                else -> -1
            }
            it.takeIf { order != -1 && order < listAdapter.itemCount }?.let { m ->
                listAdapter.items[order] = m
                listAdapter.itemsCache[order] = m
                binding.rvMovies.post { listAdapter.notifyItemChanged(order) }
            }
        }
    }
}