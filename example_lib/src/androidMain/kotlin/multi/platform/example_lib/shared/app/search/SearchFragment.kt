package multi.platform.example_lib.shared.app.search

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doOnTextChanged
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.goTo
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.extension.showToast
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.app.common.MoviesSectionHolder
import multi.platform.example_lib.shared.databinding.MoviesSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.external.extension.showSnackbar


class SearchFragment : BaseFragment<MoviesSectionViewBinding>(MoviesSectionViewBinding::inflate) {
    override fun showActionBarSearch() = true
    override fun showBottomNavBar() = true

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val moviesSectionHolder = MoviesSectionHolder(
            binding,
            viewLifecycleOwner,
            swipeRefreshLayout(),
            (requireActivity() as CoreActivity)::isInternetAvailable,
            ::dpToPx,
            ::hideKeyboard,
            ::showSnackbar,
            ::showToast,
            ::goTo
        ).also {
            actionBarSearch()?.apply {
                it.search = if (text.isNotEmpty()) text.toString().trim() else null
                it.bind(getSection(text))
            }
        }
        swipeRefreshLayout()?.setOnRefreshListener {
            moviesSectionHolder.load(0)
        }
        actionBarSearch()?.apply {
            doOnTextChanged { s, _, _, _ ->
                val searchText = if (s?.isNotEmpty() == true) s.toString().trim() else null
                if (searchText == moviesSectionHolder.search) return@doOnTextChanged
                moviesSectionHolder.search = searchText
            }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    moviesSectionHolder.bind(getSection(text))
                    return@setOnEditorActionListener true
                }
                false
            }
        }
    }

    private fun getSection(text: Editable) = Section().apply {
        title = getString(if (text.isNotEmpty()) R.string.search_title else R.string.popular_title)
        slug = if (text.isNotEmpty()) "search" else "popular_movies"
        gridCount = 2
    }
}