package multi.platform.example_lib.shared.app.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavOptions
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import multi.platform.example_lib.shared.databinding.MoviesSectionViewBinding
import multi.platform.example_lib.shared.databinding.PopularSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder

class SectionAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val swipeRefreshLayout: SwipeRefreshLayout?,
    private val isInternetAvailable: () -> Boolean,
    private val dpToPx: (Float) -> Int,
    private val hideKeyboard: () -> Unit,
    private val showSnackbar: ((String?, Boolean) -> Unit)? = null,
    private val showToast: ((String?) -> Unit)? = null,
    private val goTo: ((String, NavOptions?) -> Unit)? = null,
) : BaseAdapter<Section>() {
    var extra: Any? = null
    override val empty = Section()
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Section> {
        return when(viewType) {
            1 -> PopularSectionHolder(
                PopularSectionViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                lifecycleOwner,
                swipeRefreshLayout,
                isInternetAvailable,
                hideKeyboard,
                showSnackbar,
                showToast,
                goTo,
                extra
            )
            else -> MoviesSectionHolder(
                MoviesSectionViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                lifecycleOwner,
                swipeRefreshLayout,
                isInternetAvailable,
                dpToPx,
                hideKeyboard,
                showSnackbar,
                showToast,
                goTo,
                extra
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].slug == "popular") 1 else 2
    }
}