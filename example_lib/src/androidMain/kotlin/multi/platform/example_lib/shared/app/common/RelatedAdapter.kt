package multi.platform.example_lib.shared.app.common

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.example_lib.shared.databinding.RelatedItemBinding
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.ui.shared.R
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder

class RelatedAdapter : BaseAdapter<Movie>() {
    override var empty = Movie()
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> {
        return ViewHolder(
            RelatedItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(
        val binding: RelatedItemBinding,
    ) : BaseViewHolder<Movie>(binding.root, radius, elevation, onSelected, onTouch) {
        override val shine = binding.shineRelated
        override val cardView = binding.cvRelated

        @SuppressLint("RestrictedApi")
        override fun onLoading() {
            binding.apply {
                cvImage.isVisible = true
                tvTitle.apply {
                    setBackgroundResource(R.color.skeleton)
                }
                tvYear.apply {
                    isVisible = true
                    setBackgroundResource(R.color.skeleton)
                }
                ivPlay.apply {
                    isVisible = true
                    setImageResource(0)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        override fun onSuccess(item: Movie) {
            binding.apply {
                cvImage.isVisible = true
                ivRelated.loadImage(
                    item.posterPath ?: R.drawable.ic_image,
                    RequestOptions().centerCrop()
                )
                tvTitle.apply {
                    text = item.title
                    setBackgroundResource(android.R.color.transparent)
                }
                item.releaseDate?.let { d ->
                    tvYear.apply {
                        isVisible = true
                        text = d.split("-")[0]
                        setBackgroundResource(R.drawable.bg_white20_round8)
                    }
                }
                ivPlay.apply {
                    isVisible = true
                    setImageResource(R.drawable.ic_play_circle)
                }
            }
        }

        override fun onEmpty() {
            binding.apply {
                cvImage.isVisible = false
                tvYear.isVisible = false
                ivPlay.isVisible = false
                tvTitle.text = root.context.getString(R.string.empty)
            }
        }
    }
}