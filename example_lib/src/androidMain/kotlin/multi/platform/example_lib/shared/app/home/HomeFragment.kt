package multi.platform.example_lib.shared.app.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.goTo
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.extension.showToast
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.app.common.SectionAdapter
import multi.platform.example_lib.shared.databinding.HomeFragmentBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.external.extension.showSnackbar
import multi.platform.ui.shared.external.utility.LinearSpacingItemDecoration

class HomeFragment : BaseFragment<HomeFragmentBinding>(HomeFragmentBinding::inflate), MenuProvider {
    lateinit var sectionAdapter: SectionAdapter
    override fun actionBarTitle() = getString(R.string.menu_home)
    override fun showBottomNavBar() = true

    override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.lang -> {
                val current = LocaleUtil.retrieveAppLanguage(requireContext(), LocaleUtil.ID)
                LocaleUtil.setLocale(
                    requireContext(),
                    if (current == LocaleUtil.ID) LocaleUtil.EN else LocaleUtil.ID
                )
                activity?.finish()
                startActivity(activity?.intent)
            }
        }
        return false
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        sectionAdapter = SectionAdapter(
            viewLifecycleOwner,
            swipeRefreshLayout(),
            (requireActivity() as CoreActivity)::isInternetAvailable,
            ::dpToPx,
            ::hideKeyboard,
            ::showSnackbar,
            ::showToast,
            ::goTo
        ).also {
            it.height = -1
            it.addItem {
                val sections = mutableListOf(
                    Section().apply { slug = "popular" },
                    Section().apply {
                        slug = "latest_movies"; title = getString(R.string.latest_title)
                        limit = AppConstant.LIST_LIMIT; widthRatio = 0.35f
                    },
                    Section().apply {
                        slug = "now_playing"; title = getString(R.string.playing_title)
                        limit = AppConstant.LIST_LIMIT; widthRatio = 0.35f
                    },
                )
                it.items.addAll(sections)
                it.itemsCache.addAll(sections)
            }
        }

        swipeRefreshLayout()?.setOnRefreshListener {
            sectionAdapter.notifyDataSetChanged()
        }
        binding.apply {
            rvHome.apply {
                adapter = sectionAdapter
                isNestedScrollingEnabled = false
                if (itemDecorationCount > 0) removeItemDecorationAt(0)
                addOnScrollListener(sectionAdapter.Listener(swipeRefreshLayout()))
                addItemDecoration(
                    LinearSpacingItemDecoration(
                        LinearLayout.VERTICAL,
                        dpToPx(20f),
                        dpToPx(10f),
                        dpToPx(10f)
                    )
                )
            }
        }
    }
}