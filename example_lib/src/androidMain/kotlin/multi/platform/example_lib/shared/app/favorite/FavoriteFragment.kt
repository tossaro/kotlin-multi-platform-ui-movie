package multi.platform.example_lib.shared.app.favorite

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.extension.*
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.app.common.MoviesSectionHolder
import multi.platform.example_lib.shared.databinding.MoviesSectionViewBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.external.extension.showSnackbar


class FavoriteFragment : BaseFragment<MoviesSectionViewBinding>(MoviesSectionViewBinding::inflate) {
    override fun actionBarTitle() = getString(R.string.menu_favorites)
    override fun showBottomNavBar() = true

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val moviesSectionHolder = MoviesSectionHolder(
            binding,
            viewLifecycleOwner,
            swipeRefreshLayout(),
            (requireActivity() as CoreActivity)::isInternetAvailable,
            ::dpToPx,
            ::hideKeyboard,
            ::showSnackbar,
            ::showToast,
            ::goTo
        ).also { it.bind(Section().apply {
            slug = "loved"; orientation = LinearLayout.VERTICAL
            spacingOther = 20f
        }) }
        swipeRefreshLayout()?.setOnRefreshListener {
            moviesSectionHolder.load(0)
        }
    }

}