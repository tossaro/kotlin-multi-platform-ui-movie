package multi.platform.example_lib.shared.app.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ShareCompat
import androidx.core.view.*
import androidx.lifecycle.Lifecycle
import com.google.android.material.tabs.TabLayoutMediator
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.*
import multi.platform.example_lib.shared.R
import multi.platform.example_lib.shared.app.common.MovieCreditsViewModel
import multi.platform.example_lib.shared.app.common.MovieDetailViewModel
import multi.platform.example_lib.shared.app.common.SectionAdapter
import multi.platform.example_lib.shared.databinding.DetailFragmentBinding
import multi.platform.example_lib.shared.domain.common.entity.Section
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import multi.platform.ui.shared.R as uR
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.app.gallery.GalleryAdapter
import multi.platform.ui.shared.external.extension.showSnackbar
import multi.platform.ui.shared.external.utility.LinearSpacingItemDecoration
import multi.platform.ui.shared.external.utility.ZoomImage
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailFragment
    : BaseFragment<DetailFragmentBinding>(DetailFragmentBinding::inflate), MenuProvider {
    private var movieId = 0
    private lateinit var galleryAdapter: GalleryAdapter
    private lateinit var castAdapter: CastAdapter
    private lateinit var sectionAdapter: SectionAdapter
    private val movieDetailViewModel: MovieDetailViewModel by viewModel()
    private val movieCreditsViewModel: MovieCreditsViewModel by viewModel()

    override fun actionBarTitle() = " "
    override fun showActionBarTitleOnExpanded() = false
    override fun expandActionBar() = true
    override fun isFullScreen() = true
    override fun actionBarTopMargin() = 30

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieId = Integer.valueOf(arguments?.getString("id") ?: "0")
    }

    override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_detail, menu)
        menu.findItem(R.id.share).actionView?.setOnClickListener {
            ShareCompat.IntentBuilder(requireActivity())
                .setType("text/plain")
                .setChooserTitle(getString(R.string.share))
                .setText(getString(R.string.route_detail).replace("{id}", movieId.toString()))
                .startChooser()
        }
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return false
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        swipeRefreshLayout()?.setOnRefreshListener {
            load()
        }
        setupObserver()
        setupImagesAdapter()
        setupCastAdapter()
        setupTab()
        load()
    }

    override fun onResume() {
        super.onResume()
        swipeRefreshLayout()?.updateLayoutParams<CoordinatorLayout.LayoutParams> {
            topMargin = dpToPx(-32f)
        }
    }

    override fun onPause() {
        super.onPause()
        swipeRefreshLayout()?.updateLayoutParams<CoordinatorLayout.LayoutParams> {
            topMargin = dpToPx(0f)
        }
    }

    private fun load() {
        movieDetailViewModel.load()
        movieCreditsViewModel.load()
    }

    private fun setupObserver() {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.vmMovie = movieDetailViewModel.also {
            it.id = movieId
            it.successMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.movie.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                handleMovie(m)
            }
        }
        binding.vmCredits = movieCreditsViewModel.also {
            it.movieId = movieId
            it.successMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar(m, true)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showSnackbar(m, false)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.credits.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { cs ->
                handleCredits(cs)
            }
        }
    }

    private fun setupImagesAdapter() {
        galleryAdapter = GalleryAdapter().also { a ->
            a.onSelected = { v, i ->
                if (clRoot() != null && zoomContainer() != null && zoomImage() != null) {
                    ZoomImage(clRoot()!!, v, zoomContainer()!!, zoomImage()!!, i).show()
                }
            }
            a.showSkeletons(2)
        }
        actionBarExpandedViewPager()?.apply {
            isVisible = true
            adapter = galleryAdapter
            actionBarExpandedDotIndicator()?.let { d ->
                TabLayoutMediator(d, this) { _, _ -> }.attach()
            }
        }
    }

    private fun setupCastAdapter() {
        castAdapter = CastAdapter().also { a ->
            a.widthRatio = 0.4f
            a.height = 48
            a.showSkeletons(3)
        }
        binding.rvCast.apply {
            adapter = castAdapter
            isNestedScrollingEnabled = false
            addItemDecoration(
                LinearSpacingItemDecoration(
                    LinearLayout.HORIZONTAL,
                    dpToPx(18f),
                )
            )
        }
    }

    private fun setupTab() {
        sectionAdapter = SectionAdapter(
            viewLifecycleOwner,
            swipeRefreshLayout(),
            (requireActivity() as CoreActivity)::isInternetAvailable,
            ::dpToPx,
            ::hideKeyboard,
            ::showSnackbar,
            ::showToast,
            ::goTo
        ).also {
            it.extra = movieId
            it.addItem {
                val sections = mutableListOf(
                    Section().apply {
                        slug = "similar"; orientation = LinearLayout.VERTICAL
                        name = getString(R.string.similar); limit = AppConstant.LIST_LIMIT
                        enableNestingScroll = true
                    },
                    Section().apply {
                        slug = "recommendation"; orientation = LinearLayout.VERTICAL
                        name = getString(R.string.recommendation); limit = AppConstant.LIST_LIMIT
                        enableNestingScroll = true
                    },
                )
                it.items.addAll(sections)
                it.itemsCache.addAll(sections)
            }
        }
        binding.vpDetail.apply {
            adapter = sectionAdapter
            TabLayoutMediator(binding.tlDetail, this) { t, p ->
                t.text = sectionAdapter.items[p].name
            }.attach()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleMovie(movie: Movie?) {
        movie?.let { m ->
            if (m.trailerLink == null) movieDetailViewModel.getTrailerLink()
            galleryAdapter.apply {
                addItem {
                    items = mutableListOf(m.posterPath.toString(), m.backdropPath.toString())
                    itemsCache = mutableListOf(m.posterPath.toString(), m.backdropPath.toString())
                }
            }
            binding.apply {
                tvTitle.text = m.title
                tvDesc.text = m.overview
                tvRate.text = String.format("%.1f", m.voteAverage?.toDouble())
                m.releaseDate?.let { d ->
                    tvYear.text = d.split("-")[0]
                }

                val hour = m.runtime / 60
                val minute = m.runtime % 60
                tvRuntime.text = "${hour}h ${minute}min"

                m.genres.takeIf { it.isNotEmpty() }?.let { gs ->
                    val gss = mutableListOf<String>()
                    gs.forEach { g -> gss.add(g.name.toString()) }
                    tvGenre.text = gss.joinToString(" ⸱ ")
                }

                mbWatchTrailer.setOnClickListener {
                    m.trailerLink?.let {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                        startActivity(browserIntent)
                    }
                }

                mbLove.apply {
                    setBackgroundResource(
                        if (m.lovedOrder < 0) uR.drawable.bg_button_white_solid_rounded
                        else uR.drawable.bg_button_primary_solid_rounded
                    )
                    setIconTintResource(
                        if (m.lovedOrder < 0) uR.color.primaryVariant
                        else uR.color.onPrimary
                    )
                    setOnClickListener {
                        movieDetailViewModel.love()
                    }
                }
            }
            swipeRefreshLayout()?.isRefreshing = false
        }
    }

    private fun handleCredits(credits: List<MovieCredit>?) {
        credits?.let {
            castAdapter.apply {
                addItem {
                    items.addAll(it)
                    itemsCache.addAll(it)
                }
            }
        }
    }
}