package multi.platform.example_lib.shared.domain.movie.usecase

import multi.platform.example_lib.shared.domain.movie.MovieRepository
import multi.platform.example_lib.shared.external.enum.MoviesType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetMoviesLocalUseCase : KoinComponent {
    private val movieRepository: MovieRepository by inject()
    suspend operator fun invoke(type: MoviesType?, offset: Int, limit: Int, search: Any?) =
        movieRepository.getMoviesLocal(type, offset, limit, search)
}