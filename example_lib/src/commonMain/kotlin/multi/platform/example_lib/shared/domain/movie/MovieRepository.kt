package multi.platform.example_lib.shared.domain.movie

import multi.platform.example_lib.shared.data.movie.network.response.MovieResponse
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.domain.movie.entity.MovieVideo
import multi.platform.example_lib.shared.external.enum.MoviesType

interface MovieRepository {
    suspend fun getMovie(id: Int): Movie?
    suspend fun getMovieLocal(id: Int): Movie?
    suspend fun setMovieLocal(movie: Movie): Movie

    suspend fun getMovies(limit: Int, page: Int, search: String?): MovieResponse<List<Movie>>?
    suspend fun getMoviesLocal(type: MoviesType?, offset: Int, limit: Int, search: Any?): List<Movie>
    suspend fun setMoviesLocal(movies: List<Movie>)

    suspend fun getPopularMovies(limit: Int, page: Int): MovieResponse<List<Movie>>?
    suspend fun getNowPlayingMovies(limit: Int, page: Int): MovieResponse<List<Movie>>?
    suspend fun getUpcomingMovies(limit: Int, page: Int): MovieResponse<List<Movie>>?
    suspend fun getSimilarMovies(movieId: Int, limit: Int, page: Int): MovieResponse<List<Movie>>?
    suspend fun getRecommendationMovies(movieId: Int, limit: Int, page: Int): MovieResponse<List<Movie>>?

    suspend fun getMovieVideos(movieId: Int): MovieResponse<List<MovieVideo>>?
    suspend fun getMovieCredits(movieId: Int): MovieResponse<List<MovieCredit>>?
    suspend fun getMovieCreditsLocal(movieId: Int): List<MovieCredit>
    suspend fun setMovieCreditsLocal(movieCredits: List<MovieCredit>)
}