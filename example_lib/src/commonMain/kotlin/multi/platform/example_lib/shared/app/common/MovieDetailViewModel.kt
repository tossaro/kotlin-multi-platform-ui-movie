package multi.platform.example_lib.shared.app.common

import io.realm.kotlin.ext.copyFromRealm
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreViewModel
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.domain.movie.usecase.*
import multi.platform.example_lib.shared.external.enum.MoviesType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MovieDetailViewModel : KoinComponent, CoreViewModel() {
    private val getMovieUseCase: GetMovieUseCase by inject()
    private val getMovieLocalUseCase: GetMovieLocalUseCase by inject()
    private val setMovieLocalUseCase: SetMovieLocalUseCase by inject()
    private val getMoviesLocalUseCase: GetMoviesLocalUseCase by inject()
    private val getMovieVideosUseCase: GetMovieVideosUseCase by inject()

    var id = 0
    var isFromNetwork = true
    var useAsyncNetworkCall = true
    var movie = MutableStateFlow<Movie?>(null)

    fun load(idArg: Int? = null) {
        if (isFromNetwork) getFromNetwork(idArg)
        else getFromLocal(idArg)
    }

    private fun getFromLocal(idArg: Int? = null) {
        scope.launch {
            loadingIndicator.value = true
            val movieLocal = getMovieLocalUseCase(idArg ?: id)
            movieLocal?.takeIf { it.genres.isNotEmpty() }?.let {
                movie.value = it
                loadingIndicator.value = false
            } ?: run {
                getFromNetwork(idArg)
            }
        }
    }

    fun getFromNetwork(idArg: Int? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getMovieUseCase(idArg ?: id)
                response?.let {
                    val saved = setMovieLocalUseCase(it)
                    scope.launch {
                        movie.value = saved
                        loadingIndicator.value = false
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    movie.value = null
                    loadingIndicator.value = false
                }
            }
        }
    }

    fun getTrailerLink(idArg: Int? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getMovieVideosUseCase(idArg ?: id)
                response?.let {
                    var temp = movie.value?.copyFromRealm()
                    val trailer = it.results?.filter { r -> r.type == "Trailer" }?.first()
                    trailer?.let { t ->
                        temp?.let { m ->
                            m.trailerLink = "https://www.youtube.com/watch?v=${t.key}"
                            temp = setMovieLocalUseCase(m)
                        }
                    }
                    scope.launch {
                        movie.value = temp
                        loadingIndicator.value = false
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    movie.value = null
                    loadingIndicator.value = false
                }
            }
        }
    }

    fun love() {
        scope.launch {
            loadingIndicator.value = true
            val lovedMovies = getMoviesLocalUseCase(MoviesType.LOVED, 0, Int.MAX_VALUE, null)
            movie.value?.let {
                val temp = it.copyFromRealm()
                temp.lovedOrder = if (temp.lovedOrder < 0) lovedMovies.size - 1 + 1 else -2
                movie.value = setMovieLocalUseCase(temp)
            }
        }
    }
}