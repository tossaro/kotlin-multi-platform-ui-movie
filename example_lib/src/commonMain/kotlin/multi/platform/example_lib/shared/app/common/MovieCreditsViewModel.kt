package multi.platform.example_lib.shared.app.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreViewModel
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import multi.platform.example_lib.shared.domain.movie.usecase.GetMovieCreditsLocalUseCase
import multi.platform.example_lib.shared.domain.movie.usecase.GetMovieCreditsUseCase
import multi.platform.example_lib.shared.domain.movie.usecase.SetMovieCreditsLocalUseCase
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MovieCreditsViewModel : KoinComponent, CoreViewModel() {
    private val getMovieCreditsUseCase: GetMovieCreditsUseCase by inject()
    private val getMovieCreditsLocalUseCase: GetMovieCreditsLocalUseCase by inject()
    private val setMovieCreditsLocalUseCase: SetMovieCreditsLocalUseCase by inject()

    var movieId = 0
    var isFromNetwork = true
    var useAsyncNetworkCall = true
    var credits = MutableStateFlow<List<MovieCredit>?>(null)

    fun load(idArg: Int? = null) {
        if (isFromNetwork) getFromNetwork(idArg)
        else getFromLocal(idArg)
    }

    private fun getFromLocal(idArg: Int? = null) {
        scope.launch {
            loadingIndicator.value = true
            credits.value = getMovieCreditsLocalUseCase(idArg ?: movieId)
            loadingIndicator.value = false
        }
    }

    private fun getFromNetwork(idArg: Int? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getMovieCreditsUseCase(idArg ?: movieId)
                val movieCreditsTemp = mutableListOf<MovieCredit>()
                response?.let { r ->
                    r.crew?.let {
                        it.sortedByDescending { cs -> cs.popularity }
                            .filter { cs -> cs.knownForDepartment == "Directing"}
                            .first { c ->
                                c.profilePath?.let { p ->
                                    c.profilePath = "https://image.tmdb.org/t/p/w300$p"
                                }
                                c.movieId = idArg ?: movieId
                                movieCreditsTemp.add(c)
                            }
                    }
                    r.cast?.let {
                        it.sortedByDescending { cs -> cs.popularity }
                            .filter { cs -> cs.knownForDepartment == "Acting"}
                            .take(5).forEach { c ->
                                c.profilePath?.let { p ->
                                    c.profilePath = "https://image.tmdb.org/t/p/w300$p"
                                }
                                c.movieId = idArg ?: movieId
                                movieCreditsTemp.add(c)
                            }
                    }
                    if (movieCreditsTemp.isNotEmpty()) {
                        setMovieCreditsLocalUseCase(movieCreditsTemp)
                    }
                }
                scope.launch {
                    credits.value = movieCreditsTemp
                    loadingIndicator.value = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    credits.value = mutableListOf()
                    loadingIndicator.value = false
                }
            }
        }
    }
}