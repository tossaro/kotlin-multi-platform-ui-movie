package multi.platform.example_lib.shared.domain.common.entity

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class Section : RealmObject {
    @PrimaryKey
    var id: Int = 0
    var limit = 0
    var orientation = 0
    var slug: String? = null
    var name: String? = null
    var title: String? = null

    @SerialName("grid_count")
    var gridCount = 0
    @SerialName("spacing_start")
    var spacingStart = 20f
    @SerialName("spacing_midle")
    var spacingMidle = 18f
    @SerialName("spacing_end")
    var spacingEnd = 20f
    @SerialName("spacing_other")
    var spacingOther = 0f
    @SerialName("width_ratio")
    var widthRatio = 1f
    @SerialName("enable_nesting_scroll")
    var enableNestingScroll = false
}