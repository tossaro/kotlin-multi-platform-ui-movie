@file:UseSerializers(RealmListKSerializer::class)
package multi.platform.example_lib.shared.domain.movie.entity

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.serializers.RealmListKSerializer
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
open class Movie : RealmObject {
    @PrimaryKey
    var id: Int = 0
    var title: String? = null
    var runtime: Int = 0
    var overview: String? = null
    var genres: RealmList<MovieGenre> = realmListOf()

    @SerialName("poster_path")
    var posterPath: String? = null

    @SerialName("backdrop_path")
    var backdropPath: String? = null

    @SerialName("vote_average")
    var voteAverage: String? = null

    @SerialName("release_date")
    var releaseDate: String? = null

    //local
    var popularOrder: Int = -1
    var nowPlayingOrder: Int = -1
    var upcomingOrder: Int = -1
    var lovedOrder: Int = -1
    var searchOrder: Int = -1
    var similarOrder: Int = -1
    var recommendationOrder: Int = -1
    var trailerLink: String? = null
    @Index
    var parentId: Int = 0
}