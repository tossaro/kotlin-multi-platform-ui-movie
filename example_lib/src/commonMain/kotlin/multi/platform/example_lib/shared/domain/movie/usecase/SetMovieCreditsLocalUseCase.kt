package multi.platform.example_lib.shared.domain.movie.usecase

import multi.platform.example_lib.shared.domain.movie.MovieRepository
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SetMovieCreditsLocalUseCase : KoinComponent {
    private val movieRepository: MovieRepository by inject()
    suspend operator fun invoke(credits: List<MovieCredit>) =
        movieRepository.setMovieCreditsLocal(credits)
}