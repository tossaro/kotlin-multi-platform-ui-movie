package multi.platform.example_lib.shared.domain.movie.entity

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.serialization.Serializable

@Serializable
open class MovieGenre : RealmObject {
    @PrimaryKey
    var id: Int = 0
    var name: String? = null
}