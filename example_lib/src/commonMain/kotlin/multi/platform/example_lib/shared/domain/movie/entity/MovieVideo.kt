package multi.platform.example_lib.shared.domain.movie.entity

import kotlinx.serialization.Serializable

@Serializable
data class MovieVideo(
    var type: String? = null,
    var name: String? = null,
    var key: String? = null,
)