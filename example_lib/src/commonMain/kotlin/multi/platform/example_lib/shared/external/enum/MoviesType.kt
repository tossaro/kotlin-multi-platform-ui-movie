package multi.platform.example_lib.shared.external.enum

enum class MoviesType {
    POPULAR, UPCOMING, NOW_PLAYING, LOVED, SEARCH, UNDEFINED,
    SIMILAR, RECOMMENDATION
}