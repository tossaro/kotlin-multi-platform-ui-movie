package multi.platform.example_lib.shared.domain.movie.usecase

import multi.platform.example_lib.shared.domain.movie.MovieRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetMovieCreditsLocalUseCase : KoinComponent {
    private val movieRepository: MovieRepository by inject()
    suspend operator fun invoke(id: Int) = movieRepository.getMovieCreditsLocal(id)
}