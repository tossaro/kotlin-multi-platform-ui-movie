package multi.platform.example_lib.shared.data.movie

import io.ktor.client.call.*
import io.ktor.client.request.*
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import multi.platform.core.shared.external.utility.ApiClient
import multi.platform.example_lib.shared.data.movie.network.response.MovieResponse
import multi.platform.example_lib.shared.domain.movie.MovieRepository
import multi.platform.example_lib.shared.domain.movie.entity.MovieCredit
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.domain.movie.entity.MovieVideo
import multi.platform.example_lib.shared.external.enum.MoviesType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MovieRepositoryImpl(
    private val apiKey: String
) : KoinComponent, MovieRepository {
    private val apiClient: ApiClient by inject()
    private val realm: Realm by inject()
    private val queryId = "id = $0"

    override suspend fun getMovie(id: Int): Movie? = apiClient.client.get("/movie/$id") {
        bearerAuth(apiKey)
    }.body()

    override suspend fun getMovieLocal(id: Int): Movie? =
        realm.query<Movie>(queryId, id).first().find()

    override suspend fun setMovieLocal(movie: Movie): Movie = realm.write {
        val exist = query<Movie>(queryId, movie.id).first().find()

        var temp = Movie()
        if (exist != null) temp = exist
        else temp.id = movie.id

        temp.title = movie.title
        temp.overview = movie.overview
        temp.voteAverage = movie.voteAverage
        temp.releaseDate = movie.releaseDate

        updateMovieOrder(temp, movie)

        if (movie.parentId != 0) temp.parentId = movie.parentId
        if (movie.trailerLink != null) temp.trailerLink = movie.trailerLink
        if (movie.runtime > 0) temp.runtime = movie.runtime
        if (movie.genres.size > 0) temp.genres = movie.genres
        if (movie.posterPath?.contains("https://") == true) {
            temp.posterPath = movie.posterPath
            temp.backdropPath = movie.backdropPath
        }
        if (exist == null) copyToRealm(temp)
        temp
    }

    private fun updateMovieOrder(temp: Movie, movie: Movie) {
        if (movie.popularOrder != -1) temp.popularOrder = movie.popularOrder
        if (movie.upcomingOrder != -1) temp.upcomingOrder = movie.upcomingOrder
        if (movie.nowPlayingOrder != -1) temp.nowPlayingOrder = movie.nowPlayingOrder
        if (movie.lovedOrder != -1) temp.lovedOrder = movie.lovedOrder
        if (movie.searchOrder != -1) temp.searchOrder = movie.searchOrder
        if (movie.similarOrder != -1) temp.similarOrder = movie.similarOrder
        if (movie.recommendationOrder != -1) temp.recommendationOrder = movie.recommendationOrder
    }

    override suspend fun getMovies(
        limit: Int,
        page: Int,
        search: String?
    ): MovieResponse<List<Movie>>? =
        apiClient.client.get("/search/movie?include_adult=true&page=$page&query=$search") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getMoviesLocal(
        type: MoviesType?,
        offset: Int,
        limit: Int,
        search: Any?,
    ): List<Movie> {
        val orderKey = when (type) {
            MoviesType.POPULAR -> "popularOrder"
            MoviesType.UPCOMING -> "upcomingOrder"
            MoviesType.NOW_PLAYING -> "nowPlayingOrder"
            MoviesType.LOVED -> "lovedOrder"
            MoviesType.SEARCH -> "searchOrder"
            MoviesType.SIMILAR -> "similarOrder"
            MoviesType.RECOMMENDATION -> "recommendationOrder"
            else -> null
        }
        val searchQuery = when (search) {
            is String -> "AND (title = $1 OR overview = $1)"
            is Int -> "AND parentId = $1"
            else -> ""
        }
        return realm.query<Movie>("$orderKey >= $0 $searchQuery", offset, search).limit(limit)
            .find()
    }

    override suspend fun setMoviesLocal(movies: List<Movie>) {
        for (it in movies) setMovieLocal(it)
    }

    override suspend fun getPopularMovies(limit: Int, page: Int): MovieResponse<List<Movie>>? =
        apiClient.client.get("/movie/popular?page=$page") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getNowPlayingMovies(limit: Int, page: Int): MovieResponse<List<Movie>>? =
        apiClient.client.get("/movie/now_playing?page=$page") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getUpcomingMovies(limit: Int, page: Int): MovieResponse<List<Movie>>? =
        apiClient.client.get("/movie/upcoming?page=$page") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getSimilarMovies(
        movieId: Int,
        limit: Int,
        page: Int
    ): MovieResponse<List<Movie>>? =
        apiClient.client.get("/movie/${movieId}/similar?page=$page") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getRecommendationMovies(
        movieId: Int,
        limit: Int,
        page: Int
    ): MovieResponse<List<Movie>>? =
        apiClient.client.get("/movie/${movieId}/recommendations?page=$page") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getMovieVideos(movieId: Int): MovieResponse<List<MovieVideo>>? =
        apiClient.client.get("/movie/${movieId}/videos") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getMovieCredits(movieId: Int): MovieResponse<List<MovieCredit>>? =
        apiClient.client.get("/movie/${movieId}/credits") {
            bearerAuth(apiKey)
        }.body()

    override suspend fun getMovieCreditsLocal(movieId: Int): List<MovieCredit> =
        realm.query<MovieCredit>("movieId = $0", movieId).find()

    override suspend fun setMovieCreditsLocal(movieCredits: List<MovieCredit>) = realm.write {
        for (it in movieCredits) {
            val exist = query<MovieCredit>(queryId, it.id).first().find()
            var temp = MovieCredit()
            if (exist != null) temp = exist
            else temp.id = it.id
            temp.movieId = it.movieId
            temp.name = it.name
            temp.popularity = it.popularity
            temp.profilePath = it.profilePath
            temp.knownForDepartment = it.knownForDepartment
            if (exist == null) copyToRealm(it)
        }
    }
}