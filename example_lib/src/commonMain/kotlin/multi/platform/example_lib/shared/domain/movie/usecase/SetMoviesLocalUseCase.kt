package multi.platform.example_lib.shared.domain.movie.usecase

import multi.platform.example_lib.shared.domain.movie.MovieRepository
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SetMoviesLocalUseCase : KoinComponent {
    private val movieRepository: MovieRepository by inject()
    suspend operator fun invoke(movies: List<Movie>) = movieRepository.setMoviesLocal(movies)
}