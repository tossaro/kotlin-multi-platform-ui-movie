package multi.platform.example_lib.shared.domain.movie.entity

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class MovieCredit : RealmObject {
    @PrimaryKey
    var id: Int = 0
    @Index
    var name: String? = null
    var popularity: Double? = 0.0

    @SerialName("known_for_department")
    var knownForDepartment: String? = null

    @SerialName("profile_path")
    var profilePath: String? = null

    //
    var movieId: Int = 0
}