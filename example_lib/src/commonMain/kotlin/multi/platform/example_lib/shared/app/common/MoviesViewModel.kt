package multi.platform.example_lib.shared.app.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreViewModel
import multi.platform.example_lib.shared.domain.movie.entity.Movie
import multi.platform.example_lib.shared.domain.movie.usecase.*
import multi.platform.example_lib.shared.external.enum.MoviesType
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MoviesViewModel : KoinComponent, CoreViewModel() {
    private val getMoviesUseCase: GetMoviesUseCase by inject()
    private val getMoviesLocalUseCase: GetMoviesLocalUseCase by inject()
    private val setMoviesLocalUseCase: SetMoviesLocalUseCase by inject()
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase by inject()
    private val getUpcomingMoviesUseCase: GetUpcomingMoviesUseCase by inject()
    private val getNowPlayingMoviesUseCase: GetNowPlayingMoviesUseCase by inject()
    private val getSimilarMoviesUseCase: GetSimilarMoviesUseCase by inject()
    private val getRecommendationMoviesUseCase: GetRecommendationMoviesUseCase by inject()

    var page = 0
    var isFromNetwork = true
    var useAsyncNetworkCall = true
    var limit = 20
    var lists = MutableStateFlow<MutableList<Movie>?>(null)

    var type: MoviesType? = null
    var search: Any? = null

    fun load() {
        if (isFromNetwork && type != MoviesType.LOVED) getFromNetwork()
        else getFromLocal()
    }

    private fun getFromLocal() {
        scope.launch {
            loadingIndicator.value = true
            val moviesLocal = getMoviesLocalUseCase(
                type,
                limit * page,
                limit,
                if (type != MoviesType.SIMILAR && type != MoviesType.RECOMMENDATION) search as? String else search as? Int,
            ).toMutableList()
            if (moviesLocal.isEmpty() && type != MoviesType.LOVED) {
                getFromNetwork()
                return@launch
            } else {
                lists.value = moviesLocal
                loadingIndicator.value = false
            }
        }
    }

    @Suppress("kotlin:S108")
    private fun getFromNetwork() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = when (type) {
                    MoviesType.POPULAR -> getPopularMoviesUseCase(limit, page + 1)
                    MoviesType.UPCOMING -> getUpcomingMoviesUseCase(limit, page + 1)
                    MoviesType.NOW_PLAYING -> getNowPlayingMoviesUseCase(limit, page + 1)
                    MoviesType.SEARCH -> getMoviesUseCase(limit, page + 1, search.toString())
                    MoviesType.SIMILAR -> getSimilarMoviesUseCase(search as Int, limit, page + 1)
                    MoviesType.RECOMMENDATION -> getRecommendationMoviesUseCase(
                        search as Int, limit, page + 1
                    )
                    else -> null
                }
                val moviesTemp = mutableListOf<Movie>()
                response?.results?.let {
                    it.forEachIndexed { i, m ->
                        when (type) {
                            MoviesType.POPULAR -> m.popularOrder = (limit * page) + i
                            MoviesType.UPCOMING -> m.upcomingOrder = (limit * page) + i
                            MoviesType.NOW_PLAYING -> m.nowPlayingOrder = (limit * page) + i
                            MoviesType.LOVED -> m.lovedOrder = (limit * page) + i
                            MoviesType.SEARCH -> m.searchOrder = (limit * page) + i
                            MoviesType.SIMILAR -> {
                                m.similarOrder = (limit * page) + i
                                m.parentId = search as Int
                            }
                            MoviesType.RECOMMENDATION -> {
                                m.recommendationOrder = (limit * page) + i
                                m.parentId = search as Int
                            }
                            else -> {}
                        }
                        m.posterPath?.let { p ->
                            m.posterPath = "https://image.tmdb.org/t/p/w300$p"
                        }
                        m.backdropPath?.let { p ->
                            m.backdropPath = "https://image.tmdb.org/t/p/w300$p"
                        }
                        moviesTemp.add(m)
                    }
                    setMoviesLocalUseCase(moviesTemp)
                }
                scope.launch {
                    lists.value = moviesTemp
                    loadingIndicator.value = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    lists.value = mutableListOf()
                    loadingIndicator.value = false
                }
            }
        }
    }
}