package multi.platform.example_lib.shared.data.movie.network.response

import kotlinx.serialization.Serializable

@Serializable
data class MovieResponse<D>(
    var page: Int? = null,
    var results: D? = null,
    var cast: D? = null,
    var crew: D? = null,
)