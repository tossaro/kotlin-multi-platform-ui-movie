package multi.platform.example_lib.shared.app.onboarding

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreViewModel

class OnBoardingViewModel : CoreViewModel() {
    val onNext = MutableStateFlow(false)

    fun next() {
        scope.launch {
            onNext.value = true
        }
    }
}