package multi.platform.ui.shared.app.common

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import multi.platform.core.shared.app.common.CoreSheetFragment
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.ui.shared.R
import multi.platform.ui.shared.databinding.BaseSheetFragmentBinding
import org.koin.core.component.inject


open class BaseSheetFragment<B : ViewDataBinding>(
    @LayoutRes val layoutResId: Int
) : CoreSheetFragment() {

    lateinit var binding: B
    protected var fragmentView: View? = null
    private var _baseBinding: BaseSheetFragmentBinding? = null
    protected val baseBinding get() = _baseBinding!!
    protected val sharedPreferences: SharedPreferences by inject()
    private var bottomSheetCallback: BottomSheetCallback? = null

    /**
     * Open function for override root layout resource
     * Default: R.layout.base_fragment
     */
    protected open fun getRootLayoutRes(): Int = R.layout.base_sheet_fragment

    /**
     * Open function for override visibility loading binding
     */
    protected open fun showFullLoading(isShow: Boolean? = true) {
        hideKeyboard()
        isShow?.let { s -> baseBinding.loadingCircle.isVisible = s }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(isCancelable)
        dialog.setCanceledOnTouchOutside(isCancelable)
        dialog.setOnShowListener { d ->
            val bd = d as BottomSheetDialog
            val bottomSheet =
                bd.findViewById<FrameLayout?>(com.google.android.material.R.id.design_bottom_sheet)
            bottomSheet.let {
                val behaviour = BottomSheetBehavior.from(it as FrameLayout)
                if (minHeight() > 0) {
                    behaviour.peekHeight =
                        (resources.displayMetrics.heightPixels * minHeight()).toInt()
                    setFullHeight(it)
                }
                behaviour.maxHeight = (resources.displayMetrics.heightPixels * maxHeight()).toInt()
                behaviour.state = initialState()
                if (forceFullHeight()) {
                    behaviour.peekHeight = behaviour.maxHeight
                    behaviour.state = BottomSheetBehavior.STATE_EXPANDED
                    setFullHeight(it)
                }
            }
        }
        dialog.setOnDismissListener { d ->
            val bd = d as BottomSheetDialog
            val bottomSheet =
                bd.findViewById<FrameLayout?>(com.google.android.material.R.id.design_bottom_sheet)
            val behavior = BottomSheetBehavior.from(bottomSheet as FrameLayout)
            bottomSheetCallback?.let { behavior.removeBottomSheetCallback(it) }
        }
        return dialog
    }

    @Suppress("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _baseBinding = BaseSheetFragmentBinding.inflate(inflater, container, false)
        (fragmentView?.parent as? ViewGroup)?.removeAllViews()
        fragmentView = inflater.inflate(getRootLayoutRes(), container, false).apply {
            binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
            baseBinding.fragmentContent.removeAllViews()
            baseBinding.fragmentContent.addView(binding.root)
        }
        baseBinding.tvTitle.text = title()
        baseBinding.ivClose.visibility = if (showCloseButton()) View.VISIBLE else View.GONE
        baseBinding.ivClose.setOnClickListener { dismiss() }
        return binding.root.rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _baseBinding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        LocaleUtil.onAttach(context)
    }

}