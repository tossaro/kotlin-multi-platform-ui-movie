package multi.platform.ui.shared.app.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage
import multi.platform.ui.shared.app.common.BaseAdapter
import multi.platform.ui.shared.app.common.BaseViewHolder
import multi.platform.ui.shared.databinding.GalleryItemBinding

class GalleryAdapter : BaseAdapter<String>() {
    override var empty = ""
    override fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        return ViewHolder(
            GalleryItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    inner class ViewHolder(
        val binding: GalleryItemBinding,
    ) : BaseViewHolder<String>(binding.root, radius, elevation, onSelected, onTouch) {
        override val shine = binding.shineGallery
        override fun onSuccess(item: String) {
            binding.ivGallery.loadImage(item, RequestOptions().centerCrop())
            binding.ivGallery.clipToOutline = true
        }
    }
}