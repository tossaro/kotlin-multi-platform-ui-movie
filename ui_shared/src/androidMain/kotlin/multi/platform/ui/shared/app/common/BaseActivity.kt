@file:Suppress("SameReturnValue")

package multi.platform.ui.shared.app.common

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.internal.ViewUtils.dpToPx
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.ui.shared.BuildConfig
import multi.platform.ui.shared.R
import multi.platform.ui.shared.databinding.BaseActivityBinding
import timber.log.Timber

abstract class BaseActivity : CoreActivity() {

    private var binding: BaseActivityBinding? = null
    private var doubleBackToExitPressedOnce = false
    private var snackbar: Snackbar? = null

    /**
     * Open function for container navigation host binding
     */
    open fun navHostFragment() = binding?.contentFragment?.getFragment() as NavHostFragment

    /**
     * Open function for override action bar binding
     * Default: null
     */
    open fun clRoot() = binding?.rootView

    /**
     * Open function for override action bar binding
     * Default: null
     */
    open fun actionBar() = binding?.toolbar

    /**
     * Open function for override action bar search binding
     * Default: null
     */
    open fun actionBarSearch() = binding?.etToolbarSearch

    /**
     * Open function for override action bar layout binding
     * Default: null
     */
    open fun actionBarLayout() = binding?.ablToolbar

    /**
     * Open function for override action bar collapsing layout binding
     * Default: null
     */
    open fun actionBarCollapsingLayout() = binding?.ctlToolbar

    /**
     * Open function for override action bar description binding
     * Default: null
     */
    open fun actionBarExpandedDescription() = binding?.tvToolbarExpanded

    /**
     * Open function for override action bar auto complete binding
     * Default: null
     */
    open fun actionBarExpandedAutoComplete() = binding?.actvToolbarExpanded

    /**
     * Open function for override action bar viewpager binding
     * Default: null
     */
    open fun actionBarExpandedViewPager() = binding?.vpToolbar

    /**
     * Open function for override action bar tab layout for dot indicator binding
     * Default: null
     */
    open fun actionBarExpandedDotIndicator() = binding?.tlToolbar

    /**
     * Open function for override action bar info icon binding
     * Default: null
     */
    open fun actionBarExpandedInfoIcon() = binding?.ivToolbarInfo

    /**
     * Open function for override action bar info text binding
     * Default: null
     */
    open fun actionBarExpandedInfoText() = binding?.tvToolbarInfo

    /**
     * Open function for override bottom navigation bar binding
     * Default: null
     */
    open fun bottomNavBar() = binding?.bottomNav

    /**
     * Open function for override linear loading binding
     */
    open fun swipeRefreshLayout() = binding?.swipeContainer

    /**
     * Open function for override linear loading binding
     */
    open fun linearLoading() = binding?.loadingLinear

    /**
     * Open function for override zoom container binding
     */
    open fun zoomContainer() = binding?.llZoom

    /**
     * Open function for override zoom imageview binding
     */
    open fun zoomImage() = binding?.ivZoom

    /**
     * Open function for snackbar message
     */
    @Suppress("RestrictedApi")
    open fun showSnackbar(messageString: String, isError: Boolean, anchor: View? = null) {
        val anchorView: View? = anchor ?: clRoot()
        anchorView?.let { a ->
            snackbar?.dismiss()
            snackbar = Snackbar.make(a, messageString, Snackbar.LENGTH_LONG).apply {
                val dp11 = dpToPx(context, 11).toInt()
                val dp24 = dpToPx(context, 24).toInt()
                val dp32 = dpToPx(context, 32).toInt()
                view.background = ContextCompat.getDrawable(
                    context,
                    if (isError) R.drawable.bg_redlight_round8 else R.drawable.bg_greenlight_round8
                )
                if (anchorView is CoordinatorLayout) {
                    view.layoutParams =
                        (view.layoutParams as CoordinatorLayout.LayoutParams).apply {
                            gravity = Gravity.TOP
                            setMargins(dp24, dp32, dp24, 0)
                        }
                } else if (anchorView is FrameLayout) {
                    view.layoutParams =
                        (view.layoutParams as FrameLayout.LayoutParams).apply {
                            gravity = Gravity.TOP
                            setMargins(dp24, dp32, dp24, 0)
                        }
                }
                val textView =
                    view.findViewById<TextView?>(com.google.android.material.R.id.snackbar_text)
                textView?.let { t ->
                    t.setTextAppearance(R.style.Text_Inter_Body3_Title)
                    t.setCompoundDrawablesWithIntrinsicBounds(
                        if (isError) R.drawable.ic_info else R.drawable.ic_check,
                        0,
                        0,
                        0
                    )
                    t.setBackgroundResource(android.R.color.transparent)
                    t.compoundDrawablePadding = dp11
                }
                animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
            }
            snackbar?.show()
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleUtil.onAttach(newBase))
    }

    @Suppress("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = orientation()
        AppCompatDelegate.setDefaultNightMode(nightMode())

        binding = DataBindingUtil.setContentView(this, R.layout.base_activity)

        appBarConfiguration = AppBarConfiguration(
            topLevelDestinationIds = topLevelDestinations(),
            fallbackOnNavigateUpListener = ::onSupportNavigateUp
        )

        val navController = navHostFragment().navController
        navController.addOnDestinationChangedListener { controller, destination, _ ->
            if (appBarConfiguration.topLevelDestinations.contains(destination.id)) {
                actionBar()?.navigationIcon = null
            } else {
                actionBar()?.setNavigationIcon(R.drawable.ic_arrow_left_round)
            }
            @Suppress("RestrictedApi")
            if (BuildConfig.DEBUG && isDebugNavStack()) {
                val breadcrumb = controller
                    .backQueue
                    .map {
                        it.destination
                    }
                    .joinToString(" > ") {
                        it.displayName.split('/')[1]
                    }
                Timber.d("breadcrumb: $breadcrumb")
                Toast.makeText(this, breadcrumb, Toast.LENGTH_SHORT).show()
            }
        }

        actionBar().apply {
            setSupportActionBar(this)
            if (actionBarExpandedAutoCompleteHint() != 0) actionBarExpandedAutoComplete()?.apply {
                hint = getString(actionBarExpandedAutoCompleteHint())
            }
            if (actionBarSearchHint() != 0) actionBarSearch()?.apply {
                hint = getString(actionBarSearchHint())
            }
        }
        bottomNavBar()?.apply {
            if (bottomNavBarMenu() != 0) {
                inflateMenu(bottomNavBarMenu())
                setupWithNavController(navController)
            }
        }

        lifecycleScope.launch {
            delay(1500)
            navGraph()?.let {
                navController.graph.clear()
                navController.setGraph(it)
            }
        }
    }

    override fun onBackPressed() {
        if (navHostFragment().childFragmentManager.backStackEntryCount == 0) {
            if (doubleBackToExitPressedOnce) {
                finish()
            }

            doubleBackToExitPressedOnce = true
            Toast.makeText(
                this,
                getString(R.string.tap_to_minimize),
                Toast.LENGTH_SHORT
            ).show()

            Handler(Looper.getMainLooper()).postDelayed({
                doubleBackToExitPressedOnce = false
            }, 2000)
        } else super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        navHostFragment().findNavController().handleDeepLink(intent)
    }
}