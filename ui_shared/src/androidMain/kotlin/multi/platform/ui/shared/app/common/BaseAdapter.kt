package multi.platform.ui.shared.app.common

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.internal.ViewUtils
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.ui.shared.databinding.ErrorItemBinding
import multi.platform.ui.shared.external.utility.State

abstract class BaseAdapter<D>(
    var widthRatio: Float = 1.0f,
    var height: Int = 0,
    var radius: Int = 0,
    var elevation: Int = 0,
) : RecyclerView.Adapter<BaseViewHolder<D>>() {

    abstract val empty: D
    var items = mutableListOf<D>()
    var itemsCache = mutableListOf<D>()
    var isLastPage = false
    var state = State.LOADING
    var fetchData: (() -> Unit)? = null
    var onSelected: ((View, D) -> Unit)? = null
    var onClickMore: ((D) -> Unit)? = null
    var onTouch: ((View, MotionEvent) -> Boolean)? = null

    inner class Listener(
        private val swipeRefreshLayout: SwipeRefreshLayout? = null,
        private val fetchArg: (() -> Unit)? = null,
    ) : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val topRowVerticalPosition =
                if (recyclerView.childCount != 0) recyclerView.getChildAt(0).top else 0
            swipeRefreshLayout?.isEnabled = topRowVerticalPosition >= 0

            if (!recyclerView.canScrollVertically(1) && !isLastPage) {
                fetchArg?.invoke() ?: run { fetchData?.invoke() }
            }
        }
    }

    override fun getItemCount() = items.size

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<D> {
        val holder = if (state == State.ERROR) ErrorViewHolder(
            ErrorItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ) else initHolder(parent, viewType)
        if (widthRatio != 1.0f) holder.root.layoutParams.width =
            (parent.measuredWidth * widthRatio).toInt()
        if (height < 0) holder.root.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        else if (height > 0) holder.root.layoutParams.height =
            ViewUtils.dpToPx(holder.root.context, height).toInt()
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<D>, position: Int) {
        holder.bind(items[position], state)
    }

    abstract fun initHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<D>

    inner class ErrorViewHolder(
        val binding: ErrorItemBinding,
    ) : BaseViewHolder<D>(binding.root, radius, elevation, onSelected, onTouch) {
        override val cardView = binding.cvError

        override fun onError() {
            binding.mbError.setOnClickListener { fetchData?.invoke() }
        }
    }

    fun clear() {
        val size = items.size
        items.clear()
        itemsCache.clear()
        state = State.LOADING
        notifyItemRangeRemoved(0, size)
    }

    fun showSkeletons(size: Int = AppConstant.LIST_LIMIT) {
        clear()
        val skeletons = mutableListOf<D>()
        for (i in 0 until size) skeletons.add(empty)
        items = skeletons
        notifyItemRangeInserted(0, skeletons.size - 1)
    }

    fun addItem(process: () -> Unit) {
        if (itemsCache.isEmpty() && items.isNotEmpty()) clear()
        val sizeBefore = items.size
        process.invoke()
        val sizeAfter = items.size
        if ((sizeAfter - sizeBefore) < AppConstant.LIST_LIMIT) isLastPage = true
        if (sizeAfter == 0) {
            state = State.EMPTY
            items = mutableListOf(empty)
            notifyItemInserted(0)
        } else if (sizeAfter == 1) {
            state = State.SUCCESS
            notifyItemInserted(0)
        } else {
            state = State.SUCCESS
            notifyItemRangeInserted(sizeBefore, sizeAfter - 1)
        }
    }

    fun showError() {
        clear()
        state = State.ERROR
        items = mutableListOf(empty)
        notifyItemInserted(0)
    }
}