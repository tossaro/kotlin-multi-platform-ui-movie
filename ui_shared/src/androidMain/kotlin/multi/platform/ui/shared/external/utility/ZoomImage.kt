package multi.platform.ui.shared.external.utility

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Point
import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.core.view.isVisible
import com.bumptech.glide.request.RequestOptions
import multi.platform.core.shared.external.extension.loadImage

class ZoomImage(
    private val sourceContainerView: View,
    private val sourceView: View,
    private val targetContainerView: View,
    private val targetView: ImageView,
    private val image: String,
    private val animationDuration: Long = 250
) {
    private var currentAnimator: Animator? = null

    fun show() {
        currentAnimator?.cancel()
        targetView.loadImage(image, RequestOptions().fitCenter())

        val startBoundsInt = Rect()
        val finalBoundsInt = Rect()
        val globalOffset = Point()

        sourceView.getGlobalVisibleRect(startBoundsInt)
        sourceContainerView.getGlobalVisibleRect(finalBoundsInt, globalOffset)
        startBoundsInt.offset(-globalOffset.x, -globalOffset.y)
        finalBoundsInt.offset(-globalOffset.x, -globalOffset.y)

        val startBounds = RectF(startBoundsInt)
        val finalBounds = RectF(finalBoundsInt)

        val startScale: Float
        if ((finalBounds.width() / finalBounds.height() > startBounds.width() / startBounds.height())) {
            startScale = startBounds.height() / finalBounds.height()
            val startWidth: Float = startScale * finalBounds.width()
            val deltaWidth: Float = (startWidth - startBounds.width()) / 2
            startBounds.left -= deltaWidth.toInt()
            startBounds.right += deltaWidth.toInt()
        } else {
            startScale = startBounds.width() / finalBounds.width()
            val startHeight: Float = startScale * finalBounds.height()
            val deltaHeight: Float = (startHeight - startBounds.height()) / 2f
            startBounds.top -= deltaHeight.toInt()
            startBounds.bottom += deltaHeight.toInt()
        }

        sourceView.alpha = 0f

        animateZoomToLargeImage(startBounds, finalBounds, startScale)
        setDismissLargeImageAnimation(startBounds, startScale)
    }

    private fun animateZoomToLargeImage(startBounds: RectF, finalBounds: RectF, startScale: Float) {
        targetContainerView.isVisible = true
        targetView.pivotX = 0f
        targetView.pivotY = 0f

        currentAnimator = AnimatorSet().apply {
            play(
                ObjectAnimator.ofFloat(
                    targetView,
                    View.X,
                    startBounds.left,
                    finalBounds.left
                )
            ).apply {
                with(
                    ObjectAnimator.ofFloat(
                        targetView,
                        View.Y,
                        startBounds.top,
                        finalBounds.top
                    )
                )
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale, 1f))
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale, 1f))
            }
            duration = animationDuration
            interpolator = DecelerateInterpolator()
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    currentAnimator = null
                }

                override fun onAnimationCancel(animation: Animator) {
                    currentAnimator = null
                }
            })
            start()
        }
    }

    private fun setDismissLargeImageAnimation(startBounds: RectF, startScale: Float) {
        targetContainerView.setOnClickListener {
            currentAnimator?.cancel()

            currentAnimator = AnimatorSet().apply {
                play(
                    ObjectAnimator.ofFloat(
                        targetView,
                        View.X,
                        startBounds.left
                    )
                ).apply {
                    with(ObjectAnimator.ofFloat(targetView, View.Y, startBounds.top))
                    with(ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale))
                    with(ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale))
                }
                duration = animationDuration
                interpolator = DecelerateInterpolator()
                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        hide()
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        hide()
                    }

                    fun hide() {
                        sourceView.alpha = 1f
                        targetContainerView.isVisible = false
                        currentAnimator = null
                    }
                })
                start()
            }
        }
    }
}