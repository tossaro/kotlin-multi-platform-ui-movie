package multi.platform.ui.shared.external.utility

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridSpacingItemDecoration(
    private val gridCount: Int,
    private val start: Int,
    private val spacing: Int,
    private val end: Int
) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val column = position % gridCount
        outRect.left = spacing - column * spacing / gridCount
        outRect.right = (column + 1) * spacing / gridCount
        if (position < gridCount) outRect.top = start
        outRect.bottom = end
    }
}
