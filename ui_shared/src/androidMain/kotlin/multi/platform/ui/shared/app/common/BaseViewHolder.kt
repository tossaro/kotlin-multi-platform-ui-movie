@file:Suppress("kotlin:S1186")

package multi.platform.ui.shared.app.common

import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils
import multi.platform.ui.shared.R
import multi.platform.ui.shared.external.utility.State

abstract class BaseViewHolder<D>(
    val root: View,
    val radius: Int = 0,
    private val elevation: Int = 0,
    private val onSelected: ((View, D) -> Unit)? = null,
    private val onTouch: ((View, MotionEvent) -> Boolean)? = null
) : RecyclerView.ViewHolder(root) {
    open val shine: View? = null
    open val cardView: CardView? = null
    open fun onSuccess(item: D) {}
    open fun onError() {}
    open fun onEmpty() {}
    open fun onLoading() {}

    @Suppress("RestrictedApi")
    open fun bind(item: D, state: State? = null) {
        cardView?.let {
            it.radius = ViewUtils.dpToPx(root.context, radius)
            it.elevation = ViewUtils.dpToPx(root.context, elevation)
            if (elevation > 0) it.useCompatPadding = true
        }
        root.apply {
            setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_UP) v.performClick()
                onTouch?.invoke(v, event) ?: v.onTouchEvent(event)
            }
            setOnClickListener { onSelected?.invoke(this, item) }
        }

        state?.let { s ->
            when (s) {
                State.LOADING -> {
                    val anim = AnimationUtils.loadAnimation(root.context, R.anim.left_right)
                    shine?.let {
                        it.isVisible = true
                        it.startAnimation(anim)
                    }
                    anim.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationStart(p0: Animation?) {}
                        override fun onAnimationEnd(p0: Animation?) {
                            shine?.startAnimation(anim)
                        }

                        override fun onAnimationRepeat(p0: Animation?) {}
                    })
                    onLoading()
                }
                State.SUCCESS -> {
                    shine?.isVisible = false
                    onSuccess(item)
                }
                State.ERROR -> {
                    shine?.isVisible = false
                    onError()
                }
                State.EMPTY -> {
                    shine?.isVisible = false
                    onEmpty()
                }
            }
        }
    }
}