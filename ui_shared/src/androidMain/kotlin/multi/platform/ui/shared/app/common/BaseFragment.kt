@file:Suppress("EmptyMethod")

package multi.platform.ui.shared.app.common

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.*
import androidx.databinding.ViewDataBinding
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.ui.shared.R
import org.koin.core.component.inject


open class BaseFragment<B : ViewDataBinding>(
    private val inflaterFactory: (LayoutInflater, ViewGroup?, Boolean) -> B
) : CoreFragment() {
    val sharedPreferences: SharedPreferences by inject()

    lateinit var binding: B
    private var fragmentView: View? = null
    private var isTopInsets = true
    private var isBottomInsets = true
    private var onActionOffsetChanged: AppBarLayout.OnOffsetChangedListener? = null

    /**
     * Open function for override root coordinator layout binding
     * Default: baseActivity.clRoot()
     */
    open fun clRoot() = (requireActivity() as BaseActivity).clRoot()

    /**
     * Open function for override action bar binding
     * Default: baseActivity.actionBar()
     */
    open fun actionBar() = (requireActivity() as BaseActivity).actionBar()

    /**
     * Open function for override action bar search binding
     * Default: baseActivity.actionBarSearch()
     */
    open fun actionBarSearch() = (requireActivity() as BaseActivity).actionBarSearch()

    /**
     * Open function for override action bar layout binding
     * Default: baseActivity.actionBarLayout()
     */
    open fun actionBarLayout() = (requireActivity() as BaseActivity).actionBarLayout()

    /**
     * Open function for override action bar content scrim
     * Default: R.color.white
     */
    open fun actionBarContentScrim() = R.color.toolbarBackground

    /**
     * Open function for override action bar collapsing layout binding
     * Default: baseActivity.actionBarCollapsingLayout()
     */
    open fun actionBarCollapsingLayout() =
        (requireActivity() as BaseActivity).actionBarCollapsingLayout()

    /**
     * Open function for override action bar auto complete binding
     * Default: baseActivity.actionBarViewPager()
     */
    open fun actionBarExpandedDescription() =
        (requireActivity() as BaseActivity).actionBarExpandedDescription()

    /**
     * Open function for override action bar auto complete binding
     * Default: baseActivity.actionBarViewPager()
     */
    open fun actionBarExpandedAutoComplete() =
        (requireActivity() as BaseActivity).actionBarExpandedAutoComplete()

    /**
     * Open function for override action bar viewpager binding
     * Default: baseActivity.actionBarExpandedViewPager()
     */
    open fun actionBarExpandedViewPager() =
        (requireActivity() as BaseActivity).actionBarExpandedViewPager()

    /**
     * Open function for override action bar tab layout for dot indicator binding
     * Default: baseActivity.actionBarExpandedDotIndicator()
     */
    open fun actionBarExpandedDotIndicator() =
        (requireActivity() as BaseActivity).actionBarExpandedDotIndicator()

    /**
     * Open function for override action bar info icon binding
     * Default: baseActivity.actionBarExpandedInfoIcon()
     */
    open fun actionBarExpandedInfoIcon() =
        (requireActivity() as BaseActivity).actionBarExpandedInfoIcon()

    /**
     * Open function for override action bar info text binding
     * Default: baseActivity.actionBarInfoText()
     */
    open fun actionBarExpandedInfoText() =
        (requireActivity() as BaseActivity).actionBarExpandedInfoText()

    /**
     * Open function for override bottom navigation bar binding
     * Default: baseActivity.bottomNav()
     */
    open fun bottomNavBar() = (requireActivity() as BaseActivity).bottomNavBar()

    /**
     * Open function for override swipe refresh layout binding
     * Default: baseActivity.swipeRefreshLayout()
     */
    open fun swipeRefreshLayout() = (requireActivity() as BaseActivity).swipeRefreshLayout()

    /**
     * Open function for override enable swipe refresher
     * Default: true
     */
    open fun enableSwipeRefresher() = true

    /**
     * Open function for override linear loading binding
     * Default: baseActivity.linearLoading()
     */
    open fun linearLoading() = (requireActivity() as BaseActivity).linearLoading()

    /**
     * Open function for override zoom container binding
     * Default: baseActivity.zoomContainer()
     */
    open fun zoomContainer() = (requireActivity() as BaseActivity).zoomContainer()

    /**
     * Open function for override zoom imageview binding
     * Default: baseActivity.zoomImage()
     */
    open fun zoomImage() = (requireActivity() as BaseActivity).zoomImage()

    /**
     * Open function for override visibility loading binding
     */
    open fun showFullLoading(visibility: Boolean? = true) {
        hideKeyboard()
        visibility?.let { linearLoading()?.isVisible = it }
    }

    override fun navBarColor() = R.color.navBar
    override fun statusBarColor() = R.color.statusBar
    override fun actionBarHeight() = 300

    open fun setActionBarOffsetListener() {
        if (isActionBarOffsetListenerEnable() || !showActionBarTitleOnExpanded()) {
            var isShow = true
            var scrollRange = -1
            onActionOffsetChanged =
                AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                    if (scrollRange == -1) scrollRange = appBarLayout?.totalScrollRange!!
                    if (scrollRange + verticalOffset == 0) {
                        actionBarCollapsingLayout()?.title = actionBarTitle()
                        onCollapsedActionBar()
                        isShow = true
                    } else if (isShow) {
                        if (!showActionBarTitleOnExpanded()) actionBarCollapsingLayout()?.title =
                            " "
                        onExpandedActionBar()
                        isShow = false
                    }
                }
            actionBarLayout()?.addOnOffsetChangedListener(onActionOffsetChanged)
        }
    }

    open fun setActionBarSearchIcon() {
        actionBarSearch()?.apply {
            var icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_search)
            if (showActionBarSearchFilter()) {
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_filter)
                val isFiltered = sharedPreferences.getBoolean(AppConstant.FILTERED_KEY, false)
                val tintColor = if (isFiltered) R.color.primary else R.color.blue100
                icon?.let {
                    DrawableCompat.setTint(it, ContextCompat.getColor(requireContext(), tintColor))
                }
            }
            setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        LocaleUtil.onAttach(context)
    }

    @Suppress("ClickableViewAccessibility")
    override fun onResume() {
        super.onResume()
        activity?.window?.run {
            if (isFullScreen()) {
                isTopInsets = false
                isBottomInsets = false
            }

            setInsetsListener(this)
            setNavigationBar(this)
            setResize(this)
            statusBarColor = ContextCompat.getColor(requireContext(), statusBarColor())
            if (doesFitSystemWindows()) WindowCompat.setDecorFitsSystemWindows(this, true)
        }

        actionBarSearch()?.apply {
            isFocusable = true
            isFocusableInTouchMode = true
            isVisible = showActionBarSearch()
            setOnTouchListener { _, motionEvent ->
                if (motionEvent.action == MotionEvent.ACTION_UP
                    && this.compoundDrawables[2] != null
                    && motionEvent.rawX >= (this.right - this.compoundDrawables[2].bounds.width())
                ) {
                    onTapFilterActionBarSearch()
                    return@setOnTouchListener true
                }
                false
            }
        }
        actionBarExpandedInfoText()?.isVisible = showActionBarInfo()
        bottomNavBar()?.isVisible = showBottomNavBar()
        swipeRefreshLayout()?.apply {
            isEnabled = enableSwipeRefresher()
            updateLayoutParams<CoordinatorLayout.LayoutParams> {
                setPadding(0, 0, 0, dpToPx(if (showActionBar()) 55f else 0f))
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionBarLayout()?.apply {
            setExpanded(expandActionBar(), expandActionBar())
            isActivated = expandActionBar()
            updateLayoutParams<CoordinatorLayout.LayoutParams> {
                height = if (showActionBar()) dpToPx(actionBarHeight().toFloat()) else 0
                (behavior as? AppBarLayout.Behavior)?.setDragCallback(object :
                    AppBarLayout.Behavior.DragCallback() {
                    override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                        return expandActionBar()
                    }
                })
            }
        }
        setActionBarSearchIcon()

        actionBarCollapsingLayout()?.apply {
            title = actionBarTitle()
            setContentScrimResource(actionBarContentScrim())
        }
        setActionBarOffsetListener()
        actionBar()?.apply {
            setPadding(dpToPx(16f), 0, dpToPx(16f), 0)
            updateLayoutParams<CollapsingToolbarLayout.LayoutParams> {
                topMargin = dpToPx(
                    actionBarTopMargin().toFloat())
            }
        }
        actionBarSearch()?.apply {
            val dp11 = dpToPx(11f)
            setPadding(dp11, dp11, dp11, dp11)
            textSize = 16f
            height = dpToPx(40f)
            gravity = Gravity.START or Gravity.CENTER_VERTICAL
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflaterFactory(inflater, container, false)
        return binding.root
    }

    @Suppress("ClickableViewAccessibility")
    override fun onStop() {
        super.onStop()
        actionBarExpandedDotIndicator()?.removeAllTabs()
        actionBarExpandedViewPager()?.adapter = null
        actionBarSearch()?.setOnTouchListener(null)
        if (!showActionBarTitleOnExpanded()) {
            actionBarLayout()?.removeOnOffsetChangedListener(onActionOffsetChanged)
        }
    }
}