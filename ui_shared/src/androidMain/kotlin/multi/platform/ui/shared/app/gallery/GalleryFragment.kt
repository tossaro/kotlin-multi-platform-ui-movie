package multi.platform.ui.shared.app.gallery

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.databinding.GalleryFragmentBinding
import multi.platform.ui.shared.external.utility.State
import multi.platform.ui.shared.external.utility.ZoomImage

class GalleryFragment : BaseFragment<GalleryFragmentBinding>(GalleryFragmentBinding::inflate) {
    override fun actionBarTitle() = arguments?.getString("title") ?: " "
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val images = arguments?.getString("images", null)
        val imageAdapter = GalleryAdapter().also {
            it.height = -1
            it.state = State.SUCCESS
            it.onSelected = { v, i ->
                binding.apply {
                    ZoomImage(clGallery, v, llGalleryZoom, ivGalleryZoom, i).show()
                }
            }
            @Suppress("kotlin:S6531")
            images?.split(";")?.let { ims ->
                it.items = ims as MutableList<String>
            }
        }
        binding.rvGallery.adapter = imageAdapter
        binding.rvGallery.isNestedScrollingEnabled = false
        val lm = binding.rvGallery.layoutManager as StaggeredGridLayoutManager
        lm.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        binding.rvGallery.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val layoutParams = view.layoutParams as StaggeredGridLayoutManager.LayoutParams
                val spanIndex = layoutParams.spanIndex
                if (spanIndex == 0) outRect.right = dpToPx(4f)
                else outRect.left = dpToPx(4f)

                outRect.bottom = dpToPx(8f)
            }
        })
    }
}