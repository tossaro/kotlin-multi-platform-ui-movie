package multi.platform.ui.shared.external.utility

import android.annotation.SuppressLint
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class LinearSpacingItemDecoration(
    private val orientation: Int,
    private val spacing: Int,
    private val start: Int = 0,
    private val end: Int = 0,
    private val other: Int = 0
) : RecyclerView.ItemDecoration() {
    @SuppressLint("RestrictedApi")
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        when (parent.getChildAdapterPosition(view)) {
            0 -> {
                if (orientation == LinearLayoutManager.HORIZONTAL) {
                    outRect.left = start
                    outRect.right = spacing
                    outRect.top = other
                    outRect.bottom = other
                } else {
                    outRect.top = start
                    outRect.bottom = spacing
                    outRect.left = other
                    outRect.right = other
                }
            }

            parent.adapter!!.itemCount - 1 -> {
                if (orientation == LinearLayoutManager.HORIZONTAL) {
                    outRect.right = end
                    outRect.top = other
                    outRect.bottom = other
                } else {
                    outRect.bottom = end
                    outRect.left = other
                    outRect.right = other
                }
            }
            else -> {
                if (orientation == LinearLayoutManager.HORIZONTAL) {
                    outRect.right = spacing
                    outRect.top = other
                    outRect.bottom = other
                } else {
                    outRect.bottom = spacing
                    outRect.left = other
                    outRect.right = other
                }
            }
        }
    }
}
