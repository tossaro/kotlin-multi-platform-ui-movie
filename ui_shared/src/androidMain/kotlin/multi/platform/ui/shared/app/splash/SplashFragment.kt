package multi.platform.ui.shared.app.splash

import android.os.Bundle
import android.view.View
import multi.platform.core.shared.app.common.CoreActivity
import multi.platform.ui.shared.app.common.BaseFragment
import multi.platform.ui.shared.databinding.SplashFragmentBinding

class SplashFragment : BaseFragment<SplashFragmentBinding>(SplashFragmentBinding::inflate) {
    override fun showActionBar() = false
    override fun isFullScreen() = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvVersionValue.text = (requireActivity() as CoreActivity).appVersion()
    }
}