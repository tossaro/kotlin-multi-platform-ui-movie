package multi.platform.ui.shared.external.utility

enum class State {
    LOADING,
    SUCCESS,
    ERROR,
    EMPTY
}