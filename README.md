# Multi Platform Movie UI  | [![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/develop)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/badges/main/pipeline.svg)](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/-/commits/main) [![coverage report](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/badges/main/coverage.svg)](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/-/commits/main) [![Latest Release](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/-/badges/release.svg)](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/-/releases)

## Contents

- [Documentation](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/docs)
- [Features](#features)
- [Requirements](#requirements)
- [Example App](#example-app)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Architecture](#architecture)
- [Commands](#commands)

## Features

- Provide UI Style & Component.
- Powered by KOIN for dependency injection and using MVVM pattern with clean architecture.

## Requirements

1. Minimum Android/SDK Version: Nougat/23
2. Deployment Target iOS/SDK Version: 14.1
3. Target Android/SDK Version: Snow Cone/32
4. Compile Android/SDK Version: Snow Cone/32
5. This project is built using Android Studio version 2022.1.1 and Android Gradle 7.5
6. For iOS, please install [COCOAPODS](https://cocoapods.org/)
7. Create `properties.gradle` in your root folder, add this content:
```groovy
ext {
    gitlab = [
            publishToken: "$yourGitlabPublishToken",
            consumeToken: "$yourGitlabConsumeToken"
    ]

    deeplink = "android-app://example.app.id"

    //example app purpose
    server = [
            dev  : "\"api.themoviedb.org/3\"",
            stage: "\"api.themoviedb.org/3\"",
            beta : "\"api.themoviedb.org/3\"",
            prod : "\"api.themoviedb.org/3\"",
    ]
    //example app purpose
    apikey = "\"$yourTheMovieDbApiKey\""
}
```
> ***note:***
>- replace ***$yourGitlabPublishToken*** with your private token if you forked to your private project, otherwise leave it blank if only wanted to run.
>- replace ***$yourGitlabConsumeToken*** with your private token if you forked to your private project, otherwise leave it blank if only wanted to run.
>- replace ***$yourTheMovieDbApiKey*** with your themoviedb api key if you show list data on example app.

## Example App

- Provide hybrid usage (online/offline)
- Provide on boarding (first time show)
- Provide view holder base with state, view more manageable (sort)
- Provide skeleton loading
- Provide english - indonesia (language)
- Provide infinite scroll
- Provide carousel image using viewpager2
- Provide explore movie screen with search
- Provide list favorite collections movie
- Provide detail of movie:
    - Image slideshow with animatedzoom
    - List cast of movie
    - List similar movies
    - List recommendation movies
    - Favorite and un-favorite movie action
    - Watch trailer action (open youtube)
    - Share action
- Powered by KMM with KOIN for dependency injection and using MVVM pattern with clean architecture.

### Android

![Demo Example App Android](/resources/demo_android.gif){width=250px}

### iOS

***To be Updated Soon***

## Usage

1. Edit settings.gradle in your root folder:

```groovy
dependencyResolutionManagement {
    repositories {
        //...
        maven { url 'https://gitlab.com/api/v4/projects/47349806/packages/maven' }
    }
}
```

2. Last, add 'implementation "multi.platform.ui:ui_shared:${version}"' inside tag
   dependencies { . . . } of build.gradle app

## Project Structure

```plantuml
:ui_shared;
fork
    :example_lib;
    :example_android;
fork again
    :ui_ios;
    :example_ios;
end fork
end
```
For the high level hierarchy, the project separate into 4 main modules, which are :

### 1. [Example Android](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/example_lib)

This module contains the android library's example usage of this project.

### 2. [Example Android](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/example_android)

This module contains the android application's example usage of this project.

### 3. [Example iOS](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/example_ios)

This module contains the iOS application's example usage of this project.

### 4. [Core iOS](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/ui_ios)
This module contains iOS code that holds the iOS library, that can be injected to iOS app.

### 5. [Core Shared](https://gitlab.com/tossaro/kotlin-multi-platform-ui-movie/tree/main/ui_shared)
This module contains shared code that holds the domain and data layers and some part of the presentation logic ie.shared viewmodels.

## Architecture

This project implement
Clean [Architecture by Fernando Cejas](https://github.com/android10/Android-CleanArchitecture)

### Clean architecture

![Image Clean architecture](/resources/clean_architecture.png)

### Architectural approach

![Image Architectural approach](/resources/clean_architecture_layers.png)

### Architectural reactive approach

![Image Architectural reactive approach](/resources/clean_architecture_layers_details.png)

## Commands

Here are some useful gradle/adb commands for executing this example:

* ./gradlew clean build - Build the entire project and execute unit tests
* ./gradlew clean sonarqube - Execute sonarqube coverage report
* ./gradlew dokkaGfm - Generate documentation
* ./gradlew test[flavor][buildType]UnitTest - Execute unit tests e.g., testDevDebugUnitTest
* ./gradlew test[flavor][buildType]UnitTest create[flavor][buildType]CoverageReport - Execute unit
  tests and create coverage report e.g., createDevDebugCoverageReport
* ./gradlew assemble[flavor][buildType] - Create apk file e.g., assembleDevDebug
* ./gradlew :ui_shared:assembleXCFramework - Generate XCFramework for iOS
* ./gradlew publish - Publish - Publish to repository packages (MAVEN)