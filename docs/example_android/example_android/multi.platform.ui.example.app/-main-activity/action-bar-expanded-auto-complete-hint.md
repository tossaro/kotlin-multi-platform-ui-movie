//[example_android](../../../index.md)/[multi.platform.ui.example.app](../index.md)/[MainActivity](index.md)/[actionBarExpandedAutoCompleteHint](action-bar-expanded-auto-complete-hint.md)

# actionBarExpandedAutoCompleteHint

[androidJvm]\

public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[actionBarExpandedAutoCompleteHint](action-bar-expanded-auto-complete-hint.md)()
