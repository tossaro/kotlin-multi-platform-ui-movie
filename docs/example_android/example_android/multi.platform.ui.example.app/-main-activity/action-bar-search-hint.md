//[example_android](../../../index.md)/[multi.platform.ui.example.app](../index.md)/[MainActivity](index.md)/[actionBarSearchHint](action-bar-search-hint.md)

# actionBarSearchHint

[androidJvm]\

public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[actionBarSearchHint](action-bar-search-hint.md)()
