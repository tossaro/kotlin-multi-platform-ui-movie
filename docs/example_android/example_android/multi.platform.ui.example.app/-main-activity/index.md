//[example_android](../../../index.md)/[multi.platform.ui.example.app](../index.md)/[MainActivity](index.md)

# MainActivity

[androidJvm]\
public final class [MainActivity](index.md) extends BaseActivity

## Constructors

| | |
|---|---|
| [MainActivity](-main-activity.md) | [androidJvm]<br>public [MainActivity](index.md)[MainActivity](-main-activity.md)() |

## Functions

| Name | Summary |
|---|---|
| [actionBarExpandedAutoCompleteHint](action-bar-expanded-auto-complete-hint.md) | [androidJvm]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[actionBarExpandedAutoCompleteHint](action-bar-expanded-auto-complete-hint.md)() |
| [actionBarSearchHint](action-bar-search-hint.md) | [androidJvm]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[actionBarSearchHint](action-bar-search-hint.md)() |
| [appVersion](app-version.md) | [androidJvm]<br>public [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[appVersion](app-version.md)() |
| [bottomNavBarMenu](bottom-nav-bar-menu.md) | [androidJvm]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[bottomNavBarMenu](bottom-nav-bar-menu.md)() |
| [navGraph](nav-graph.md) | [androidJvm]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[navGraph](nav-graph.md)() |
| [topLevelDestinations](top-level-destinations.md) | [androidJvm]<br>public [Set](https://developer.android.com/reference/kotlin/java/util/Set.html)&lt;[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)&gt;[topLevelDestinations](top-level-destinations.md)() |
