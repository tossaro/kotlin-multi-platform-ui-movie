//[example_android](../../../index.md)/[multi.platform.ui.example](../index.md)/[ExampleApplication](index.md)/[appVersion](app-version.md)

# appVersion

[androidJvm]\

public [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[appVersion](app-version.md)()
