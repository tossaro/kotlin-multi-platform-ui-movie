//[example_lib](../../../index.md)/[multi.platform.example_lib.shared.data.common.network.request](../index.md)/[RefreshTokenReq](index.md)/[getMsisdn](get-msisdn.md)

# getMsisdn

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMsisdn](get-msisdn.md)()
