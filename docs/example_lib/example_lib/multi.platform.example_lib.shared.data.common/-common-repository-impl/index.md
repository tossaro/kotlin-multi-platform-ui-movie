//[example_lib](../../../index.md)/[multi.platform.example_lib.shared.data.common](../index.md)/[CommonRepositoryImpl](index.md)

# CommonRepositoryImpl

[common]\
public final class [CommonRepositoryImpl](index.md) implements CommonRepository

## Constructors

| | |
|---|---|
| [CommonRepositoryImpl](-common-repository-impl.md) | [common]<br>public [CommonRepositoryImpl](index.md)[CommonRepositoryImpl](-common-repository-impl.md)(ApiClientapiClient) |

## Functions

| Name | Summary |
|---|---|
| [refreshToken](refresh-token.md) | [common]<br>public CoreResponse&lt;Ticket&gt;[refreshToken](refresh-token.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)refreshToken, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)phone) |
