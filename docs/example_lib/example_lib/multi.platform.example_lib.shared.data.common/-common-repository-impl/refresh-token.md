//[example_lib](../../../index.md)/[multi.platform.example_lib.shared.data.common](../index.md)/[CommonRepositoryImpl](index.md)/[refreshToken](refresh-token.md)

# refreshToken

[common]\

public CoreResponse&lt;Ticket&gt;[refreshToken](refresh-token.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)refreshToken, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)phone)
