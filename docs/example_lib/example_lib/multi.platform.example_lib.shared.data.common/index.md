//[example_lib](../../index.md)/[multi.platform.example_lib.shared.data.common](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CommonRepositoryImpl](-common-repository-impl/index.md) | [common]<br>public final class [CommonRepositoryImpl](-common-repository-impl/index.md) implements CommonRepository |
