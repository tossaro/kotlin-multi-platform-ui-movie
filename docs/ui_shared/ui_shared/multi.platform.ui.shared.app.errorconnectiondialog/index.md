//[ui_shared](../../index.md)/[multi.platform.ui.shared.app.errorconnectiondialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ErrorConnectionDialogFragment](-error-connection-dialog-fragment/index.md) | [android]<br>public final class [ErrorConnectionDialogFragment](-error-connection-dialog-fragment/index.md) extends [BaseDialogFragment](../multi.platform.ui.shared.app.common/-base-dialog-fragment/index.md)&lt;&lt;Error class: unknown class&gt;&gt; |
