//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.errorconnectiondialog](../index.md)/[ErrorConnectionDialogFragment](index.md)/[isCancelable](is-cancelable.md)

# isCancelable

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isCancelable](is-cancelable.md)()
