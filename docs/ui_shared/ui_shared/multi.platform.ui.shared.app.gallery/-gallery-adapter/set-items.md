//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.gallery](../index.md)/[GalleryAdapter](index.md)/[setItems](set-items.md)

# setItems

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItems](set-items.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;items)
