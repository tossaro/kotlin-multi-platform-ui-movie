//[ui_shared](../../../../index.md)/[multi.platform.ui.shared.app.gallery](../../index.md)/[GalleryAdapter](../index.md)/[ViewHolder](index.md)/[bind](bind.md)

# bind

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[bind](bind.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)item)
