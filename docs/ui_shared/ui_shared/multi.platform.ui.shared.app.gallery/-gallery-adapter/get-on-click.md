//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.gallery](../index.md)/[GalleryAdapter](index.md)/[getOnClick](get-on-click.md)

# getOnClick

[android]\

public final Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), [String](https://developer.android.com/reference/kotlin/java/lang/String.html), [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[getOnClick](get-on-click.md)()
