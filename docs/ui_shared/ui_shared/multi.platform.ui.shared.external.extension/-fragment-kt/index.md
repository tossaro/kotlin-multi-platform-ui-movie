//[ui_shared](../../../index.md)/[multi.platform.ui.shared.external.extension](../index.md)/[FragmentKt](index.md)

# FragmentKt

[android]\
public final class [FragmentKt](index.md)

## Functions

| Name | Summary |
|---|---|
| [showErrorSnackbar](show-error-snackbar.md) | [android]<br>public final static [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showErrorSnackbar](show-error-snackbar.md)([Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html)$self, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString)<br>Extension for show error snackbar |
| [showSnackbar](show-snackbar.md) | [android]<br>public final static [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSnackbar](show-snackbar.md)([Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html)$self, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isError)<br>Extension for show snackbar |
| [showSuccessSnackbar](show-success-snackbar.md) | [android]<br>public final static [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSuccessSnackbar](show-success-snackbar.md)([Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html)$self, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString)<br>Extension for show success snackbar |
