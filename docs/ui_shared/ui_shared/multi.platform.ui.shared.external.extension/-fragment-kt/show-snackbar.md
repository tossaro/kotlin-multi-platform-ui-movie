//[ui_shared](../../../index.md)/[multi.platform.ui.shared.external.extension](../index.md)/[FragmentKt](index.md)/[showSnackbar](show-snackbar.md)

# showSnackbar

[android]\

public final static [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSnackbar](show-snackbar.md)([Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html)$self, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isError)

Extension for show snackbar
