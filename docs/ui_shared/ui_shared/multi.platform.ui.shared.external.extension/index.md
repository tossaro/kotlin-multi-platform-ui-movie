//[ui_shared](../../index.md)/[multi.platform.ui.shared.external.extension](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [FragmentKt](-fragment-kt/index.md) | [android]<br>public final class [FragmentKt](-fragment-kt/index.md) |
