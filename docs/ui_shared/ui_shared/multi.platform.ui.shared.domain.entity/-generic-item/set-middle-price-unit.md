//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setMiddlePriceUnit](set-middle-price-unit.md)

# setMiddlePriceUnit

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setMiddlePriceUnit](set-middle-price-unit.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)middlePriceUnit)
