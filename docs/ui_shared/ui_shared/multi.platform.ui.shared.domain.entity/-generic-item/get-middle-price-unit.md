//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getMiddlePriceUnit](get-middle-price-unit.md)

# getMiddlePriceUnit

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMiddlePriceUnit](get-middle-price-unit.md)()
