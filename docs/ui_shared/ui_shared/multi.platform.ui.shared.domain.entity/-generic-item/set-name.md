//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setName](set-name.md)

# setName

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
