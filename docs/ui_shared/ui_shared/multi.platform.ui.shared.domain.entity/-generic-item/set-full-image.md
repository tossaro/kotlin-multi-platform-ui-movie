//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setFullImage](set-full-image.md)

# setFullImage

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFullImage](set-full-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)fullImage)
