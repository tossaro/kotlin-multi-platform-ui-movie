//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setId](set-id.md)

# setId

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id)
