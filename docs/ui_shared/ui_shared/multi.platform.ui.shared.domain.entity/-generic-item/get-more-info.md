//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getMoreInfo](get-more-info.md)

# getMoreInfo

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMoreInfo](get-more-info.md)()
