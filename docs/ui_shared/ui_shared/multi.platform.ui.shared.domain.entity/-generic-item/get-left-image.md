//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getLeftImage](get-left-image.md)

# getLeftImage

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getLeftImage](get-left-image.md)()
