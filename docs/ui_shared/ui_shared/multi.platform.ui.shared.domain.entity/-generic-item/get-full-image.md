//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getFullImage](get-full-image.md)

# getFullImage

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getFullImage](get-full-image.md)()
