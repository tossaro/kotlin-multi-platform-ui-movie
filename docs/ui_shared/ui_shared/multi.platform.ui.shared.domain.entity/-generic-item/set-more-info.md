//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setMoreInfo](set-more-info.md)

# setMoreInfo

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setMoreInfo](set-more-info.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)moreInfo)
