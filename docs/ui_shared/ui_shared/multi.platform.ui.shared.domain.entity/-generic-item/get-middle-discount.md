//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getMiddleDiscount](get-middle-discount.md)

# getMiddleDiscount

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMiddleDiscount](get-middle-discount.md)()
