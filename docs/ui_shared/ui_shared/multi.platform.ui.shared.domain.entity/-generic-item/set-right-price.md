//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setRightPrice](set-right-price.md)

# setRightPrice

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setRightPrice](set-right-price.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)rightPrice)
