//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getSubtitle](get-subtitle.md)

# getSubtitle

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getSubtitle](get-subtitle.md)()
