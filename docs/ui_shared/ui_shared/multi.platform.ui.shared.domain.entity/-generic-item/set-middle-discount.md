//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setMiddleDiscount](set-middle-discount.md)

# setMiddleDiscount

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setMiddleDiscount](set-middle-discount.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)middleDiscount)
