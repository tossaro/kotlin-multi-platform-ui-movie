//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setDescriptionIconRes](set-description-icon-res.md)

# setDescriptionIconRes

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setDescriptionIconRes](set-description-icon-res.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)descriptionIconRes)
