//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setTitleOverlay](set-title-overlay.md)

# setTitleOverlay

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTitleOverlay](set-title-overlay.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)titleOverlay)
