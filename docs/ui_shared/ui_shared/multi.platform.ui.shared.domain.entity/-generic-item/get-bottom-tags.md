//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getBottomTags](get-bottom-tags.md)

# getBottomTags

[common]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getBottomTags](get-bottom-tags.md)()
