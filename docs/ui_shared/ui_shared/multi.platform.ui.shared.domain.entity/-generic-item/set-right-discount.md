//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setRightDiscount](set-right-discount.md)

# setRightDiscount

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setRightDiscount](set-right-discount.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)rightDiscount)
