//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setSubtitleIconRes](set-subtitle-icon-res.md)

# setSubtitleIconRes

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSubtitleIconRes](set-subtitle-icon-res.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)subtitleIconRes)
