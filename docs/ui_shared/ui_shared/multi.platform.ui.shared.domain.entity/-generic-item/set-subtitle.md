//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setSubtitle](set-subtitle.md)

# setSubtitle

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSubtitle](set-subtitle.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)subtitle)
