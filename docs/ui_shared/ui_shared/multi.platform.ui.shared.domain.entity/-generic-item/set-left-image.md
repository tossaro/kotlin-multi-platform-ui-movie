//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setLeftImage](set-left-image.md)

# setLeftImage

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setLeftImage](set-left-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)leftImage)
