//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getRightPrice](get-right-price.md)

# getRightPrice

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getRightPrice](get-right-price.md)()
