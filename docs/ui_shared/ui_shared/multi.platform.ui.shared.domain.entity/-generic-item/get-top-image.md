//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getTopImage](get-top-image.md)

# getTopImage

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTopImage](get-top-image.md)()
