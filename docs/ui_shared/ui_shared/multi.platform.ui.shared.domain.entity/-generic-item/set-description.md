//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setDescription](set-description.md)

# setDescription

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setDescription](set-description.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)description)
