//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getMiddlePrice](get-middle-price.md)

# getMiddlePrice

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMiddlePrice](get-middle-price.md)()
