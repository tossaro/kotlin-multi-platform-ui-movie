//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getTitle](get-title.md)

# getTitle

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTitle](get-title.md)()
