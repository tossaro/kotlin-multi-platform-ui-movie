//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setTitle](set-title.md)

# setTitle

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTitle](set-title.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)title)
