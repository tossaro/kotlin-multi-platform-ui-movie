//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getSubtitleIconRes](get-subtitle-icon-res.md)

# getSubtitleIconRes

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getSubtitleIconRes](get-subtitle-icon-res.md)()
