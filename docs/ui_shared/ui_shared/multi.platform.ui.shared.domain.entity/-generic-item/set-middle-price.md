//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setMiddlePrice](set-middle-price.md)

# setMiddlePrice

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setMiddlePrice](set-middle-price.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)middlePrice)
