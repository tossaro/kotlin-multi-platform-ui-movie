//[ui_shared](../../index.md)/[multi.platform.ui.shared.domain.entity](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GenericItem](-generic-item/index.md) | [common]<br>public final class [GenericItem](-generic-item/index.md) |
