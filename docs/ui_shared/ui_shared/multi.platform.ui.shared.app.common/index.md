//[ui_shared](../../index.md)/[multi.platform.ui.shared.app.common](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [BaseActivity](-base-activity/index.md) | [android]<br>public abstract class [BaseActivity](-base-activity/index.md) extends CoreActivity |
| [BaseDialogFragment](-base-dialog-fragment/index.md) | [android]<br>public class [BaseDialogFragment](-base-dialog-fragment/index.md)&lt;B extends ViewDataBinding&gt; extends CoreDialogFragment |
| [BaseFragment](-base-fragment/index.md) | [android]<br>public class [BaseFragment](-base-fragment/index.md)&lt;B extends ViewDataBinding&gt; extends CoreFragment |
| [BaseSheetFragment](-base-sheet-fragment/index.md) | [android]<br>public class [BaseSheetFragment](-base-sheet-fragment/index.md)&lt;B extends ViewDataBinding&gt; extends CoreSheetFragment |
| [GenericAdapter](-generic-adapter/index.md) | [android]<br>public final class [GenericAdapter](-generic-adapter/index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[GenericAdapter.ViewHolder](-generic-adapter/-view-holder/index.md)&gt; |
