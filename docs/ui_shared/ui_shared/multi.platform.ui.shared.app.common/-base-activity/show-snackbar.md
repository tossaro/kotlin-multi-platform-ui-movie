//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseActivity](index.md)/[showSnackbar](show-snackbar.md)

# showSnackbar

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSnackbar](show-snackbar.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isError, [View](https://developer.android.com/reference/kotlin/android/view/View.html)anchor)

Open function for snackbar message
