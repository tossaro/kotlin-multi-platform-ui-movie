//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseActivity](index.md)/[actionBarExpandedViewPager](action-bar-expanded-view-pager.md)

# actionBarExpandedViewPager

[android]\

public &lt;Error class: unknown class&gt;[actionBarExpandedViewPager](action-bar-expanded-view-pager.md)()

Open function for override action bar viewpager binding Default: null
