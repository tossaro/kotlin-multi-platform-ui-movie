//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[GenericAdapter](-generic-adapter.md)

# GenericAdapter

[android]\

public [GenericAdapter](index.md)[GenericAdapter](-generic-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)radius, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)elevation)
