//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[showInfo](show-info.md)

# showInfo

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showInfo](show-info.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)desc, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)button)
