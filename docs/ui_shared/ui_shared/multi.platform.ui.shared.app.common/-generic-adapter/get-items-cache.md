//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[getItemsCache](get-items-cache.md)

# getItemsCache

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;GenericItem&gt;[getItemsCache](get-items-cache.md)()
