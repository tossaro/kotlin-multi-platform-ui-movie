//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[getItemViewType](get-item-view-type.md)

# getItemViewType

[android]\

public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getItemViewType](get-item-view-type.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position)
