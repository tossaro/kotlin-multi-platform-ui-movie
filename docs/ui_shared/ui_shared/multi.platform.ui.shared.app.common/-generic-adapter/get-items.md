//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[getItems](get-items.md)

# getItems

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;GenericItem&gt;[getItems](get-items.md)()
