//[ui_shared](../../../../index.md)/[multi.platform.ui.shared.app.common](../../index.md)/[GenericAdapter](../index.md)/[Listener](index.md)/[onScrolled](on-scrolled.md)

# onScrolled

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onScrolled](on-scrolled.md)([RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html)recyclerView, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)dx, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)dy)
