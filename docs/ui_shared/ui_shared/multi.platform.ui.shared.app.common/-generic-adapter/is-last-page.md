//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[isLastPage](is-last-page.md)

# isLastPage

[android]\

public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isLastPage](is-last-page.md)()
