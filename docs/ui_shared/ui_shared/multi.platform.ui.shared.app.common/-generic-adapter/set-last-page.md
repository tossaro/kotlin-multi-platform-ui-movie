//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[setLastPage](set-last-page.md)

# setLastPage

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setLastPage](set-last-page.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isLastPage)
