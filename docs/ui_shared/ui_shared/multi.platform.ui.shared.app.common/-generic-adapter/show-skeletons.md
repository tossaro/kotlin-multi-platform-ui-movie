//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[showSkeletons](show-skeletons.md)

# showSkeletons

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSkeletons](show-skeletons.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)size)
