//[ui_shared](../../../../index.md)/[multi.platform.ui.shared.app.webview](../../index.md)/[WebViewFragment](../index.md)/[Companion](index.md)

# Companion

[android]\
public class [Companion](index.md)

## Functions

| Name | Summary |
|---|---|
| [getTYPE_PAYMENT](get-t-y-p-e_-p-a-y-m-e-n-t.md) | [android]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTYPE_PAYMENT](get-t-y-p-e_-p-a-y-m-e-n-t.md)() |

## Properties

| Name | Summary |
|---|---|
| [INSTANCE](index.md#1042770472%2FProperties%2F1613350253) | [android]<br>public final static [WebViewFragment.Companion](index.md)[INSTANCE](index.md#1042770472%2FProperties%2F1613350253) |
| [TYPE_PAYMENT](index.md#-1829471652%2FProperties%2F1613350253) | [android]<br>private final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[TYPE_PAYMENT](index.md#-1829471652%2FProperties%2F1613350253) |
