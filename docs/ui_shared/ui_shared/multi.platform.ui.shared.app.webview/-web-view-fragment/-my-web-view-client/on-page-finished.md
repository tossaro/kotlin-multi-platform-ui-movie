//[ui_shared](../../../../index.md)/[multi.platform.ui.shared.app.webview](../../index.md)/[WebViewFragment](../index.md)/[MyWebViewClient](index.md)/[onPageFinished](on-page-finished.md)

# onPageFinished

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onPageFinished](on-page-finished.md)([WebView](https://developer.android.com/reference/kotlin/android/webkit/WebView.html)view, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)url)
