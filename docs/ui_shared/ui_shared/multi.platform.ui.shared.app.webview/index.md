//[ui_shared](../../index.md)/[multi.platform.ui.shared.app.webview](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WebViewFragment](-web-view-fragment/index.md) | [android]<br>public final class [WebViewFragment](-web-view-fragment/index.md) extends [BaseFragment](../multi.platform.ui.shared.app.common/-base-fragment/index.md)&lt;&lt;Error class: unknown class&gt;&gt; |
