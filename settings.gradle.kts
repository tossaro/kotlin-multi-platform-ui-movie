pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        maven("https://jitpack.io")
    }
}
dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        maven("https://jitpack.io")
        maven("https://gitlab.com/api/v4/projects/38836420/packages/maven")
    }
}
rootProject.name = "Movie UI"
include(":ui_shared")
include(":example_lib")
include(":example_android")
